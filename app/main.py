"""The CRUDL routes for the ESAP-DB projects, datasets and tables."""
import logging
from pathlib import Path

from dotenv import load_dotenv
from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware

from .apis.v0 import api_v0_router
from .config import settings
from .exceptions import (
    ESAPDBResourceExistsError,
    ESAPDBResourceNotFoundError,
    ESAPDBValidationError,
)

logger = logging.getLogger(__name__)


BASE_PATH = Path(__file__).parents[1].absolute()
load_dotenv(BASE_PATH / '.env')

app = FastAPI(
    title=settings.PROJECT_NAME, openapi_url=f'{settings.API_V0_STR}/openapi.json'
)

# Set all CORS enabled origins
if settings.BACKEND_CORS_ORIGINS:
    app.add_middleware(
        CORSMiddleware,
        allow_origins=[str(origin) for origin in settings.BACKEND_CORS_ORIGINS],
        allow_credentials=True,
        allow_methods=['*'],
        allow_headers=['*'],
    )

app.include_router(api_v0_router, prefix=settings.API_V0_STR)

for _exc in (
    ESAPDBResourceExistsError,
    ESAPDBResourceNotFoundError,
    ESAPDBValidationError,
):
    app.exception_handler(_exc)(_exc.error_handler)
