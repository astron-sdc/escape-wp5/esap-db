"""This module contains helpers to manipulate SQL queries."""
import re
from typing import Callable, Optional

import sqlparse

from .exceptions import ESAPDBNotImplementedError, ESAPDBValidationError

REGEX_SELECT_CLAUSE = re.compile(
    r'[\s;]*(SELECT .*?)[\s;]*$', re.DOTALL | re.IGNORECASE
)


class SQLQuery:
    """Class representing a SQL query."""

    def __init__(self, query: str) -> None:
        """The `SQLQuery` constructor."""
        self.query = query
        self.parsed = self._validate(query)
        self.source_project = self._get_source_project()
        self.normalized = self._normalize()

    def _validate(self, query: str) -> sqlparse.sql.Statement:
        def _validate_project(node: sqlparse.sql.Token) -> None:
            if (
                isinstance(node, sqlparse.sql.Identifier)
                and self._length_identifier(node) > 5
            ):
                raise ESAPDBValidationError(
                    'The table identifier is not of the form '
                    f"'project.dataset.table': {node.value}"
                )

        match = REGEX_SELECT_CLAUSE.match(query)
        if match is None:
            raise ESAPDBValidationError(
                'The SQL query statement does not start with a SELECT clause.'
            )
        parsed_statements = sqlparse.parse(match[1])
        if len(parsed_statements) > 1:
            raise ESAPDBValidationError(
                'Multiple statements are not allowed in a query.'
            )

        parsed = parsed_statements[0]
        self._walk_tree(parsed, _validate_project)
        return parsed

    def _get_source_project(self) -> Optional[str]:
        """The project that contains the referenced tables."""

        def _add_project(node: sqlparse.sql.Token) -> None:
            if (
                isinstance(node, sqlparse.sql.Identifier)
                and self._length_identifier(node) > 3
            ):
                projects.add(node.tokens[0].value)

        projects: set[str] = set()
        self._walk_tree(self.parsed, _add_project)
        if not projects:
            return None
        if len(projects) > 1:
            raise ESAPDBNotImplementedError(
                'It is not yet possible to combine tables from different projects: '
                f'{", ".join(sorted(projects))}'
            )
        return projects.pop()

    def _normalize(self) -> str:
        """The modified query, in which the projects are stripped."""

        def _strip_project(node: sqlparse.sql.Token) -> None:
            if (
                isinstance(node, sqlparse.sql.Identifier)
                and self._length_identifier(node) > 3
            ):
                del node.tokens[:-3]

        self._walk_tree(self.parsed, _strip_project)
        return ''.join(str(_) for _ in self.parsed)

    def _walk_tree(
        self, node: sqlparse.sql.Token, func: Callable[[sqlparse.sql.Token], None]
    ) -> None:
        tokens = getattr(node, 'tokens', ())
        for child in tokens:
            func(child)
            self._walk_tree(child, func)

    @staticmethod
    def _length_identifier(node: sqlparse.sql.Identifier) -> int:
        """Accounts for 'x.y.z AS t'."""
        length = 0
        for token in node.tokens:
            if str(token.ttype) == 'Token.Text.Whitespace':
                break
            length += 1
        return length
