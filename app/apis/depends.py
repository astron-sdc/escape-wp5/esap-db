"""Dependencies that are injected in the routes."""
from collections.abc import Iterator

from sqlalchemy.engine import Connection
from sqlalchemy.orm import Session

from ..db.dbadmin import begin_session
from ..db.dbprojects import begin_connection


def get_session() -> Iterator[Session]:
    """Returns an admin database transaction session.

    At the end of the request, the transaction is automatically committed and the
    session closed. When using this session, the method `session.flush` needs to be run
    in order to get the server-side generated values of the added resources.
    """
    with begin_session() as session:
        yield session


def get_connection(project: str) -> Iterator[Connection]:
    """Returns a transaction connection associated with a project."""
    with begin_connection(project) as connection:
        yield connection
