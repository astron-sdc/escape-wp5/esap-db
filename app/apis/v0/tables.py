"""Definitions of the endpoints related the tables."""
import base64
import logging
import os
import pickle
import re
from io import StringIO
from pathlib import Path
from typing import Callable, Literal, Optional, Union

import pandas as pd
import requests
from fastapi import APIRouter, Depends, HTTPException, Response, status
from sqlalchemy import text
from sqlalchemy.engine import Connection
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.orm import Session

from ... import db
from ...db.dbadmin import begin_session
from ...db.dbprojects import begin_connection, project_engines
from ...db.dbprojects.operations import drop_table, select_table_column_names
from ...exceptions import ESAPDBNotImplementedError, ESAPDBValidationError
from ...helpers import uid
from ...schemas import (
    BodyCreateTableFrom,
    BodyCreateTableFromESAPGatewayQuery,
    Project,
    Table,
    TableCreate,
    normalize_identifier,
)
from ...sql import SQLQuery
from ..depends import get_connection, get_session

logger = logging.getLogger(__name__)
router = APIRouter()
HEADERS_JSON = {'Accept': 'application/json'}
REGEX_CONTENT_DISPOSITION_NAME = re.compile(r'\bname\s*=\s*"(.*)"')
REGEX_CONTENT_DISPOSITION_FILENAME = re.compile(r'\bfilename\s*=\s*"(.*)"')
REGEX_VALID_FORMAT = re.compile('[a-z]')


@router.get(
    '/projects/{project}/datasets/{dataset}/tables',
    summary='Lists the tables of a dataset.',
)
def list_tables(
    project: str, dataset: str, session: Session = Depends(get_session)
) -> list[Table]:
    """Lists the tables in a dataset."""
    return db.table.list_by_dataset(session, f'{project}.{dataset}')


@router.post(
    '/projects/{project}/datasets/{dataset}/tables',
    summary='Creates a table from a mapping.',
    description='The keys correspond to the column names and the values to the column '
    'values.',
)
def create_table_from(
    project: str,
    dataset: str,
    body: BodyCreateTableFrom,
    if_exists: Literal['fail', 'replace', 'append'] = 'fail',
    connection: Connection = Depends(get_connection),
    session: Session = Depends(get_session),
) -> Table:
    """Creates a table from a data source."""
    if body.name is not None:
        parts = body.name.split('.')
        if len(parts) == 3 and parts[0] != project:
            raise HTTPException(
                status.HTTP_400_BAD_REQUEST,
                f'The project specified in the path ({project}) differs '
                f'from that specified in the request body ({parts[0]}).',
            )
        if len(parts) >= 2 and parts[-2] != dataset:
            raise HTTPException(
                status.HTTP_400_BAD_REQUEST,
                f'The dataset specified in the path ({dataset}) differs'
                f' from that specified in the request body ({parts[-2]}).',
            )

    dataframe_getter: Optional[Callable[[], pd.DataFrame]]

    if body.content is not None:
        assert body.name is not None
        *_, table = body.name.split('.')
        description = body.description

        def dataframe_getter() -> pd.DataFrame:
            assert body.content is not None
            return _get_dataframe_content(body.content)

    elif body.path is not None:
        description = body.description or body.path

        if body.name:
            # the request can be deferred after the Table creation
            *_, table = body.name.split('.')

            def dataframe_getter() -> pd.DataFrame:
                return _get_dataframe_path(body)[1]

        else:
            # the request cannot be deferred: it is needed for the table name
            table, df = _get_dataframe_path(body)

            def dataframe_getter() -> pd.DataFrame:
                return df

    else:
        assert body.query is not None

        description = body.description or body.query
        if body.name is not None:
            *_, table = body.name.split('.')
        else:
            table = uid('anonymous')
        dataframe_getter = None

    table_name = f'{project}.{dataset}.{table}'

    if if_exists == 'fail':
        table_create = TableCreate(name=table_name, description=description)
        table_out = db.table.create(session, table_create)
        if_exists = 'replace'
    else:
        table_out = db.table.get_by_name(session, table_name)

    if dataframe_getter is not None:
        df = dataframe_getter()
        _write_dataframe(df, project, dataset, table, if_exists)

    else:
        assert body.query is not None
        sql_query = SQLQuery(body.query)
        source_project = sql_query.source_project
        normalized_query = sql_query.normalized
        if source_project is None or source_project == project:
            _create_table_as_current_project(
                project, normalized_query, f'{dataset}.{table}'
            )
        else:
            project_ = db.project.get_by_name(session, source_project)
            print(f'{project_=}, {normalized_query=}, {source_project=}')
            df = _create_table_as(project_, normalized_query)
            _write_dataframe(df, project, dataset, table, if_exists)

    return table_out


def _get_dataframe_content(content: Union[str, dict[str, list]]) -> pd.DataFrame:
    if isinstance(content, str):
        # FIXME arbitrary code execution, data cannot be trusted
        return pickle.loads(base64.b64decode(content))

    return pd.DataFrame(content)


def _get_dataframe_path(body: BodyCreateTableFrom) -> tuple[str, pd.DataFrame]:
    assert body.path is not None
    response = requests.get(body.path)
    response.raise_for_status()
    table = normalize_identifier(_infer_name_from_response(body.name, response))
    format = _infer_format_from_response(body.params.pop('format', None), response)

    if format == 'csv':
        df = pd.read_csv(StringIO(response.text), **body.params)
    else:
        raise ESAPDBValidationError(
            f"Cannot handle format '{format}'. Valid formats are: csv."
        )

    return table, df


def _infer_name_from_response(name: Optional[str], response: requests.Response) -> str:
    name = name or None
    if name:
        return name.split('.')[-1]

    url = Path(response.url)
    if url.suffix[1:] in {'csv'}:
        return url.stem

    content_disposition = response.headers.get('Content-Disposition', '')
    match = REGEX_CONTENT_DISPOSITION_NAME.search(content_disposition)
    if match and match[1]:
        return match[1]

    match = REGEX_CONTENT_DISPOSITION_FILENAME.search(content_disposition)
    if match and match[1]:
        return Path(match[1]).stem

    raise ESAPDBValidationError(
        'The table name cannot be inferred from the remote resource.'
    )


def _infer_format_from_response(
    format: Optional[str], response: requests.Response
) -> str:
    format = format or None
    if format:
        return format.lower()

    content_type = response.headers.get('Content-Type')
    if content_type:
        content_type = content_type.split(';')[0].strip().split('/')[-1]
        if content_type == 'csv':
            return 'csv'

    path = Path(response.url)
    if _valid_format(format := path.suffix[1:].lower()):
        return format

    content_disposition = response.headers.get('Content-Disposition', '')
    match = REGEX_CONTENT_DISPOSITION_FILENAME.search(content_disposition)
    if match and match[1]:
        path = Path(match[1])
        if _valid_format(format := path.suffix[1:].lower()):
            return format

    raise ESAPDBValidationError(
        'The format of the remote resource could not be inferred.'
    )


def _write_dataframe(
    dataframes: pd.DataFrame, project: str, dataset: str, table: str, if_exists: str
) -> None:
    dataframes.to_sql(
        table,
        project_engines[project],
        schema=dataset,
        if_exists=if_exists,
        index=False,
    )


def _valid_format(value: str) -> bool:
    """Some heuristics to check if a suffix is valid."""
    return len(value) <= 8 and REGEX_VALID_FORMAT.search(value) is not None


def _create_table_as_current_project(
    project_name: str, query: str, destination: str
) -> None:
    stmt = f'CREATE TABLE {destination} AS {query}'
    with begin_connection(project_name) as connection:
        connection.execute(text(stmt))


def _create_table_as(source_project: Project, query: str) -> pd.DataFrame:
    if source_project.type == 'user':
        raise ESAPDBNotImplementedError(
            'It is not yet possible to create a table using a query on another project.'
        )
    assert source_project.type == 'vo'
    service = db.vo_project.get_by_name(source_project.name)
    results = service.search(query)
    table = results.to_table()

    # Remove columns of type 'object' FIXME many false positives
    columns = [_ for _ in table.colnames if table.dtype[_].char == 'O']
    table.remove_columns(columns)
    return table.to_pandas()


@router.post(
    '/projects/{project}/esap-gateway-operations',
    summary='Performs an ESAP Gateway query and stores it in a table.',
)
def create_esap_gateway_operation(
    project: str,
    body: BodyCreateTableFromESAPGatewayQuery,
) -> Table:
    """Creates an operation that queries the ESAP API Gateway."""
    *_, dataset, table = body.name.split('.')
    requests_session = requests.Session()
    requests_session.headers.update(HEADERS_JSON)
    requests_session.trust_env = False

    page = 1
    while True:
        try:
            _create_esap_gateway_operation_paginated(
                project, dataset, table, requests_session, body.query, page
            )
        except StopIteration:
            break
        page += 1

    table_create = TableCreate(
        name=f'{project}.{dataset}.{table}', description=body.description
    )

    try:
        with begin_session() as session:
            return db.table.create(session, table_create)
    except SQLAlchemyError:
        # XXX drop table in project database
        raise


def _create_esap_gateway_operation_paginated(
    project: str,
    dataset: str,
    table: str,
    session: requests.Session,
    query: dict,
    page: int,
) -> None:
    if query['archive_uri'] == 'apertif':
        query['page'] = page
        query['page_size'] = 500
    data = session.get(f'{os.environ["ESAP_HOST"]}/esap-api/query/query', params=query)
    results = data.json()
    if query['archive_uri'] == 'apertif':
        data = results.pop('results')
        if page == 1:
            if_exists = 'replace'
        else:
            if_exists = 'append'

    dataframes = pd.DataFrame.from_records(data)
    dataframes.to_sql(
        table,
        project_engines[project],
        schema=dataset,
        if_exists=if_exists,
        index=False,
    )

    if query['archive_uri'] == 'apertif':
        if page == results['pages']:
            raise StopIteration


@router.get(
    '/projects/{project}/datasets/{dataset}/tables/{table}',
    summary='Gets a table from a dataset.',
)
def get_table(
    project: str,
    dataset: str,
    table: str,
    session: Session = Depends(get_session),
) -> Table:
    """Gets a table from a dataset."""
    return db.table.get_by_name(session, f'{project}.{dataset}.{table}')


@router.post(
    '/projects/{project}/datasets/{dataset}/tables/{table}:getLength',
    summary='Gets the number of rows in the table.',
)
def get_length_table(
    project: str,
    dataset: str,
    table: str,
    session: Session = Depends(get_session),
    connection: Connection = Depends(get_connection),
) -> int:
    """Gets a table from a dataset."""
    project_ = db.project.get_by_name(session, project)
    if project_.type == 'vo':
        raise ESAPDBNotImplementedError('Cannot get the number of rows yet')
    stmt = text(f'SELECT COUNT(*) FROM {dataset}.{table}')
    result = connection.execute(stmt).scalar_one()
    return result


@router.get(
    '/projects/{project}/datasets/{dataset}/tables/{table}/content',
    summary='Gets the content of a table.',
)
def get_table_content(
    project: str,
    dataset: str,
    table: str,
    connection: Connection = Depends(get_connection),
) -> list[dict]:
    """Returns the whole table as json."""
    column_names = select_table_column_names(connection, dataset, table)
    stmt = text(f'SELECT * FROM {dataset}.{table}')
    result = connection.execute(stmt)
    content = result.all()
    return [dict(zip(column_names, row)) for row in content]


@router.get(
    '/projects/{project}/datasets/{dataset}/tables/{table}/column-names',
    summary='Gets the column names of a table.',
)
def get_table_column_names(
    dataset: str, table: str, connection: Connection = Depends(get_connection)
) -> list[str]:
    """Returns the column names of a table."""
    return select_table_column_names(connection, dataset, table)


@router.delete(
    '/projects/{project}/datasets/{dataset}/tables/{table}', summary='Deletes a table.'
)
def delete_table(
    project: str,
    dataset: str,
    table: str,
    *,
    session: Session = Depends(get_session),
    connection: Connection = Depends(get_connection),
) -> Response:
    """Deletes a table."""
    table_name = f'{project}.{dataset}.{table}'
    db.table.delete_by_name(session, table_name)
    drop_table(connection, table_name)
    return Response(status_code=status.HTTP_204_NO_CONTENT)
