"""Definitions of the endpoints /api/v0."""
from fastapi import APIRouter

from . import datasets, projects, tables, users

api_v0_router = APIRouter()
api_v0_router.include_router(users.router, prefix='/users', tags=['users'])
api_v0_router.include_router(projects.router, prefix='/projects', tags=['projects'])
api_v0_router.include_router(datasets.router, tags=['datasets'])
api_v0_router.include_router(tables.router, tags=['tables'])
