"""Definitions of the endpoints related the users."""
import logging

from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from app.schemas import User

from ...db.dbadmin import DBUser, begin_session

logger = logging.getLogger(__name__)
router = APIRouter()


@router.post('/', response_model=User)
def create_user(*, session: Session = Depends(begin_session), user: User) -> DBUser:
    """Creates a user."""
    db_user = DBUser(
        first_name=user.first_name,
        last_name=user.last_name,
        email=user.email,
        is_superuser=False,
    )
    session.add(db_user)
    return db_user
