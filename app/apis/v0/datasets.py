"""Definitions of the endpoints related the datasets."""
import logging

from fastapi import APIRouter, Depends, HTTPException, Response, status
from sqlalchemy.engine import Connection
from sqlalchemy.orm import Session

from ... import db
from ...db.dbprojects.operations import create_schema, drop_schema
from ...schemas import Dataset, DatasetCreate
from ..depends import get_connection, get_session

logger = logging.getLogger(__name__)
router = APIRouter()


@router.get('/projects/{project}/datasets', summary='Lists the datasets of a project.')
def list_datasets(
    project: str, session: Session = Depends(get_session)
) -> list[Dataset]:
    """Lists the datasets belonging to a project."""
    return db.dataset.list_by_project(session, project)


@router.post('/projects/{project}/datasets', summary='Creates a dataset in a project.')
def create_dataset(
    project: str,
    dataset_in: DatasetCreate,
    connection: Connection = Depends(get_connection),
    session: Session = Depends(get_session),
) -> Dataset:
    """Creates a dataset in a project."""
    if '.' not in dataset_in.name:
        dataset_in.name = f'{project}.{dataset_in.name}'
    elif (project_in := dataset_in.name.split('.')[0]) != project:
        raise HTTPException(
            status.HTTP_400_BAD_REQUEST,
            f'The project specified in the path ({project}) differs '
            f'from that specified in the request body ({project_in}).',
        )

    dataset_out = db.dataset.create(session, dataset_in)
    create_schema(connection, dataset_in.name)
    return dataset_out


@router.get(
    '/projects/{project}/datasets/{dataset}', summary='Gets a dataset of a project.'
)
def get_dataset(
    project: str, dataset: str, session: Session = Depends(get_session)
) -> Dataset:
    """Gets a dataset."""
    name = f'{project}.{dataset}'
    return db.dataset.get_by_name(session, name)


@router.delete('/projects/{project}/datasets/{dataset}', summary='Deletes a dataset.')
def delete_dataset(
    project: str,
    dataset: str,
    *,
    session: Session = Depends(get_session),
    connection: Connection = Depends(get_connection),
) -> Response:
    """Deletes an empty dataset."""
    dataset_name = f'{project}.{dataset}'
    tables = db.table.list_by_dataset(session, dataset_name)
    if tables:
        raise HTTPException(
            status.HTTP_409_CONFLICT,
            f"The dataset '{dataset_name}' cannot be deleted because it is not empty.",
        )
    db.dataset.delete_by_name(session, dataset_name)
    drop_schema(connection, dataset_name)
    return Response(status_code=status.HTTP_204_NO_CONTENT)
