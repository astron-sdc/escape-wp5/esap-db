"""Definitions of the endpoints related the projects."""
import logging

from fastapi import APIRouter, Depends, HTTPException, Response, status
from pydantic import SecretStr
from sqlalchemy import update
from sqlalchemy.future import select
from sqlalchemy.orm import Session

from ... import db
from ...db.dbadmin import DBProjectServer
from ...db.dbprojects.operations import create_database, drop_database
from ...helpers import fix_sqlalchemy2_stubs_non_nullable_column
from ...schemas import Project, ProjectCreate
from ..depends import get_session

logger = logging.getLogger(__name__)
router = APIRouter()


@router.get('', summary='Lists the projects.')
def list_projects(*, session: Session = Depends(get_session)) -> list[Project]:
    """Lists the projects visible to a user."""
    return db.project.list_(session)


@router.post('', summary='Creates a project.')
def create_project(
    *, session: Session = Depends(get_session), project_in: ProjectCreate
) -> Project:
    """Creates a new project."""
    is_user_project = project_in.uri is None

    if is_user_project:
        server = _find_best_project_server(session, project_in.max_size)
        project_in.project_server_id = server.id
        project_in.uri = SecretStr(f'{server.uri}/{project_in.name}')

    project_out = db.project.create(session, project_in)

    if is_user_project:
        _update_server_available_size(session, server, -project_in.max_size)
        server_uri = fix_sqlalchemy2_stubs_non_nullable_column(server.uri)
        create_database(server_uri, project_in.name)

    return project_out


def _find_best_project_server(session: Session, max_size: int) -> DBProjectServer:
    stmt = select(DBProjectServer).where(DBProjectServer.available_size >= max_size)
    server = session.execute(stmt).scalars().first()
    if server is None:
        raise HTTPException(
            status_code=status.HTTP_507_INSUFFICIENT_STORAGE,
            detail='No database has enough storage for this project.',
        )
    return server


def _update_server_available_size(
    session: Session, server: DBProjectServer, size: int
) -> None:
    stmt = (
        update(DBProjectServer)
        .where(DBProjectServer.id == server.id)
        .values(available_size=DBProjectServer.available_size + size)
    )
    session.execute(stmt)
    session.flush()


@router.get('/{project}', summary='Gets a project.')
def get_project(project: str, *, session: Session = Depends(get_session)) -> Project:
    """Gets a project visible to a user."""
    return db.project.get_by_name(session, project)


@router.delete('/{project}', summary='Deletes a project.')
def delete_project(
    project: str, *, session: Session = Depends(get_session)
) -> Response:
    """Deletes an empty project."""
    datasets = db.dataset.list_by_project(session, project)
    if datasets:
        raise HTTPException(
            status.HTTP_409_CONFLICT,
            f"The project '{project}' cannot be deleted because it is not empty.",
        )
    server_id = db.project.get_by_name(session, project).project_server_id
    db.project.delete_by_name(session, project)
    server = db.project_server.get_by_id(session, server_id)
    drop_database(server.uri.get_secret_value(), project)
    return Response(status_code=status.HTTP_204_NO_CONTENT)
