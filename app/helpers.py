"""Some helper functions for the API controlers."""
from typing import Optional, TypeVar
from uuid import uuid4

T = TypeVar('T')


def fix_sqlalchemy2_stubs_non_nullable_column(arg: Optional[T]) -> T:
    """Non-nullable model columns are of type Optional[...] for mypy.

    It was correctly handled by sqlalchemy-stubs, but it is not yet the case for
    sqlalchemy2-stubs==0.0.2a4.
    """
    assert arg is not None
    return arg


def uid(arg: str = '') -> str:
    """Random project, dataset or table identifier."""
    if arg:
        arg += '_'
    return arg + str(uuid4()).replace('-', '_')
