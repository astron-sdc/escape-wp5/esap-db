"""The Pydantic classes to represent a table."""
import re

from pydantic import BaseModel, validator

from .helpers import IDENTIFIER_MAX_LENGTH, IDENTIFIER_REGEX_STR

TABLE_REGEX = re.compile(rf'^({IDENTIFIER_REGEX_STR}\.){{0,2}}{IDENTIFIER_REGEX_STR}$')


class TableBase(BaseModel):
    """The Table schema."""

    name: str
    description: str = ''


class TableCreate(TableBase):
    """Schema to create a table."""

    @validator('name')
    def check_name(cls, v: str) -> str:
        """Table name validator."""
        *_, table_name = v.split('.')
        if len(table_name) > IDENTIFIER_MAX_LENGTH:
            raise ValueError(
                f'The table name length is greater than {IDENTIFIER_MAX_LENGTH}: '
                f'{table_name}'
            )
        if TABLE_REGEX.match(v) is None:
            raise ValueError(
                f'Invalid table name: {v}.'
                "Valid characters are alphanumerical, '_' or '$'."
            )
        return v

    class Config:
        schema_extra = {
            'example': {
                'name': 'my_project.my_dataset.my_table',
                'description': 'My first table 😁',
            }
        }


class Table(TableBase):
    """The Table schema mapped to the database."""

    id: int
    project_id: int
    dataset_id: int

    class Config:
        orm_mode = True
