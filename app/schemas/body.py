"""The schemas of the data handled by ESAP-DB.

These schemas are used to deserialize data from or serialize data to an ESAP-DB
client.
"""

from typing import Any, Optional, Union

from pydantic import BaseModel, Field, root_validator


class BodyCreateTableFrom(BaseModel):
    """The body schema to create a table.

    Current available input sources are:
        * a mapping column name / list of values
        * a base64 encoded pickled Pandas DataFrame
        * a SELECT query
    """

    name: Optional[str] = None
    description: str = ''
    content: Union[None, str, dict[str, list]] = None
    path: Optional[str] = None
    query: Optional[str] = None
    params: dict[str, Any] = Field(default_factory=dict)

    @root_validator()
    def check_data_source(cls, values: dict) -> dict:
        """Ensures there is only one data source."""
        nsource = sum(values[_] is not None for _ in ['content', 'path', 'query'])
        if nsource == 0:
            raise ValueError('No data source is specified in the request body.')
        if nsource > 1:
            raise ValueError(
                'Ambiguous data source: only one of the following sources must be '
                "set in the request body: 'content', 'path', 'query'."
            )
        return values

    @root_validator()
    def check_name(cls, values: dict) -> dict:
        """Ensures the name is set when content is provided."""
        if values['content'] is not None and not values['name']:
            raise ValueError('The table name is not specified.')
        return values

    class Config:
        schema_extra = {
            'example': {
                'name': 'my_dataset.simple_table',
                'description': 'Example of columnar input.',
                'content': {
                    'x': [1, 2, 3, 4],
                    'y': ['a', 'b', 'c', None],
                },
            },
        }


class BodyCreateTableFromESAPGatewayQuery(BaseModel):
    """The ESAPGatewayQuery schema."""

    name: str
    description: str = ''
    query: dict

    class Config:
        schema_extra = {
            'example': {
                'name': 'my_dataset.my_table_apertif',
                'description': 'My apertif query.',
                'query': {
                    'level': 'raw',
                    'collection': 'imaging',
                    'ra': 342.16,
                    'dec': 33.94,
                    'fov': 10,
                    'archive_uri': 'apertif',
                },
            },
        }
