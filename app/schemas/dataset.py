"""The Pydantic classes to represent a dataset."""
import re

from pydantic import BaseModel, validator

from .helpers import IDENTIFIER_MAX_LENGTH, IDENTIFIER_REGEX_STR

DATASET_REGEX = re.compile(rf'^({IDENTIFIER_REGEX_STR}\.)?{IDENTIFIER_REGEX_STR}$')


class DatasetBase(BaseModel):
    """The Dataset schema."""

    name: str
    description: str = ''


class DatasetCreate(DatasetBase):
    """Schema to create a dataset."""

    @validator('name')
    def check_name(cls, v: str) -> str:
        """Dataset name validator."""
        *_, dataset_name = v.split('.')
        if len(dataset_name) > IDENTIFIER_MAX_LENGTH:
            raise ValueError(
                f'The dataset name length is greater than {IDENTIFIER_MAX_LENGTH}: '
                f'{dataset_name}'
            )
        if DATASET_REGEX.match(v) is None:
            raise ValueError(
                f'Invalid dataset name: {v}.'
                "Valid characters are alphanumerical, '_' or '$'."
            )
        return v

    class Config:
        schema_extra = {
            'example': {
                'name': 'my_project.my_dataset',
                'description': 'My first dataset 😃',
            }
        }


class Dataset(DatasetBase):
    """The Dataset schema mapped to the database."""

    id: int
    project_id: int

    class Config:
        orm_mode = True
