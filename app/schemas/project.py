"""The Pydantic classes to represent a project."""
import re
from typing import Optional

from pydantic import BaseModel, SecretStr, validator

from .helpers import IDENTIFIER_MAX_LENGTH, IDENTIFIER_REGEX_STR

PROJECT_REGEX = re.compile(f'^{IDENTIFIER_REGEX_STR}$')


class ProjectBase(BaseModel):
    """The Project base schema."""

    project_server_id: Optional[int] = None
    name: str
    description: str = ''
    type: str = 'user'
    uri: Optional[SecretStr] = None
    max_size: int = 10 * 2 ** 30


class ProjectCreate(ProjectBase):
    """Schema to create a project."""

    @validator('name')
    def check_name(cls, v: str) -> str:
        """Project name validator."""
        if len(v) > IDENTIFIER_MAX_LENGTH:
            raise ValueError(
                f'The project name length is greater than {IDENTIFIER_MAX_LENGTH}: {v}'
            )
        if PROJECT_REGEX.match(v) is None:
            raise ValueError(
                f'Invalid project name: {v}. '
                "Valid characters are alphanumerical, '_' or '$'."
            )
        return v

    class Config:
        schema_extra = {
            'example': {
                'name': 'my_project',
                'description': 'My first project 😀',
                'max_size': 10 * 2 ** 30,
            }
        }


def uri_encoder(secret: SecretStr) -> str:
    """Hides password if present in URI."""
    value = secret.get_secret_value()
    if '@' in value:
        return 'postgresql://********:********@' + value.split('@', 1)[1]
    return value


class Project(ProjectBase):
    """The Project schema mapped to the database."""

    id: int
    uri: SecretStr

    class Config:
        orm_mode = True
        json_encoders = {SecretStr: uri_encoder}
