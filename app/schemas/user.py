"""The Pydantic classes representing a user."""
from pydantic import BaseModel


class User(BaseModel):
    """The User schema."""

    first_name: str
    last_name: str
    email: str

    class Config:
        orm_mode = True
