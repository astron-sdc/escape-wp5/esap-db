"""The Pydantic classes to represent a project server."""
from typing import Optional

from pydantic import BaseModel, SecretStr, validator


class ProjectServerBase(BaseModel):
    """The project server base schema."""

    uri: SecretStr
    max_size: Optional[int] = None
    available_size: Optional[int] = None


class ProjectServerCreate(ProjectServerBase):
    """Schema to create a project server."""

    @validator('available_size', always=True)
    def set_available_size(cls, v: Optional[int], values: dict) -> int:
        """By default, the project server is assumed to be empty."""
        if v is None:
            v = values['max_size']
        return v

    class Config:
        schema_extra = {
            'example': {
                'uri': 'postgreql://********:********@db.datasource.org',
                'max_size': 2 ** 50,
                'available_size': 0,
            }
        }


class ProjectServer(ProjectServerBase):
    """The Project schema mapped to the database."""

    id: int

    class Config:
        orm_mode = True
        json_encoders = {
            SecretStr: lambda v: 'postgresql://********:********@'
            + v.get_secret_value().split('@', 1)[1],
        }
