"""Module containing common variables."""
import re

IDENTIFIER_MAX_LENGTH = 63
IDENTIFIER_REGEX_STR = '[a-zA-Z_][a-zA-Z0-9_$]*'

_INVALID_CHARACTER_REGEX = re.compile('[^a-zA-Z0-9_$]')


def normalize_identifier(identifier: str) -> str:
    """Returns a valid identifier for a resource.

    Substitutes the invalid characters with an underscore.
    """
    identifier = _INVALID_CHARACTER_REGEX.sub('_', identifier)
    if identifier and identifier[0].isdigit():
        identifier = '_' + identifier
    return identifier
