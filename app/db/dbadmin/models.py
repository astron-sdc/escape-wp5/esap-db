"""The object-relational mapping for the ESAP-DB admin database."""
from sqlalchemy import BigInteger, Boolean, Column, ForeignKey, Integer, String
from sqlalchemy.orm import declarative_base

Base = declarative_base()


class DBProjectServer(Base):
    """The model representing an ESAP-DB database server to store the projects."""

    __tablename__ = 'project_servers'
    id = Column(Integer, primary_key=True, index=True)
    uri = Column(String, unique=True, nullable=False)
    max_size = Column(BigInteger, nullable=False)
    available_size = Column(BigInteger, nullable=False)


class DBProject(Base):
    """The model representing an ESAP-DB project."""

    __tablename__ = 'projects'
    id = Column(Integer, primary_key=True, index=True)
    project_server_id = Column(Integer, ForeignKey('project_servers.id'))
    name = Column(String(256), unique=True, index=True, nullable=False)
    description = Column(String, nullable=False)
    type = Column(String(32), nullable=False)
    uri = Column(String, nullable=False)
    max_size = Column(BigInteger, nullable=False)


class DBDataset(Base):
    """The model representing an ESAP-DB dataset."""

    __tablename__ = 'datasets'
    id = Column(Integer, primary_key=True, index=True)
    project_id = Column(Integer, ForeignKey('projects.id'), nullable=False)
    name = Column(String(513), unique=True, index=True, nullable=False)
    description = Column(String, nullable=False)


class DBTable(Base):
    """The model representing an ESAP-DB table."""

    __tablename__ = 'tables'
    id = Column(Integer, primary_key=True, index=True)
    project_id = Column(Integer, ForeignKey('projects.id'), nullable=False)
    dataset_id = Column(Integer, ForeignKey('datasets.id'), nullable=False)
    name = Column(String(513), unique=True, index=True, nullable=False)
    description = Column(String, nullable=False)


class DBUser(Base):
    """The model representing an ESAP-DB user."""

    __tablename__ = 'users'
    id = Column(Integer, primary_key=True, index=True)
    first_name = Column(String, nullable=False)
    last_name = Column(String, nullable=False)
    email = Column(String, nullable=False)
    is_superuser = Column(Boolean, nullable=False)
