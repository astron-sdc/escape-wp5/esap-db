"""This module provides accessors to the admin database.

To perform a transaction on the admin database, the context manager :begin_session:
can be used.
"""
from contextlib import contextmanager
from typing import Iterator

from sqlalchemy import create_engine
from sqlalchemy.orm import Session, sessionmaker

from ...config import settings

assert settings.SQLALCHEMY_DBADMIN_URI is not None

engine = create_engine(settings.SQLALCHEMY_DBADMIN_URI, pool_pre_ping=True, future=True)
AdminSession = sessionmaker(autocommit=False, autoflush=False, bind=engine, future=True)


@contextmanager
def begin_session() -> Iterator[Session]:
    """Returns an admin database transaction session.

    On exit, the transaction is automatically committed and the session closed.
    When using this session, the method `session.flush` needs to be run in order
    to get the server-side generated values of the added resources.
    """
    with AdminSession.begin() as session:
        yield session
