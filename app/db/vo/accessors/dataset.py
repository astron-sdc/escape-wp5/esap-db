from ....schemas import Dataset  # type: ignore
from .project import vo_project_accessor


class VODatasetAccessor:
    def list_by_project(self, service_name: str) -> list[Dataset]:
        service = vo_project_accessor.get_by_name(service_name)
        query = 'SELECT schema_name, description FROM tap_schema.schemas ORDER BY schema_name'
        result = service.search(query)
        return [
            Dataset(id=0, project_id=0, name=n, description=d)
            for n, d in zip(result['schema_name'], result['description'])
        ]

    def get_by_name(self, service_name: str, schema: str) -> Dataset:
        service = vo_project_accessor.get_by_name(service_name)
        query = (
            f"SELECT description FROM tap_schema.schemas WHERE schema_name='{schema}'"
        )
        description = service.search(query)['description'][0]
        return Dataset(id=0, project_id=0, name=schema, description=description)


vo_dataset_accessor = VODatasetAccessor()
