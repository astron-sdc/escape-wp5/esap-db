import pyvo

from ....schemas import ProjectCreate, normalize_identifier  # type: ignore

SERVICES: dict = {}


class VOProjectAccessor:
    def get_by_name(self, name: str) -> pyvo.registry.regtap.RegistryResource:
        return SERVICES[name]

    def fetch_all(self) -> list[ProjectCreate]:
        projects = []
        global SERVICES
        SERVICES = {}
        for service in pyvo.regsearch(servicetype='tap'):
            name = service['ivoid'].removeprefix('ivo://')
            identifier = normalize_identifier(name).lower()
            SERVICES[identifier] = service

        # duplicates are now removed
        for identifier, service in SERVICES.items():
            description = service['res_description']
            uri = service['access_url']
            project = ProjectCreate(
                name=identifier, description=description, type='vo', uri=uri
            )
            projects.append(project)
        return projects


vo_project_accessor = VOProjectAccessor()
vo_project_accessor.fetch_all()
