"""Provides the dataset accessor object."""
from sqlalchemy.future import select
from sqlalchemy.orm import Session

from ...schemas.dataset import Dataset, DatasetCreate
from ...schemas.project import Project
from ..dbadmin import DBDataset
from .base import AccessorWithNameBase
from .project import project_accessor


class AccessorDataset(AccessorWithNameBase[DBDataset, Dataset, DatasetCreate]):
    """The Dataset accessor class."""

    resource_type = 'dataset'

    def list_by_project(self, session: Session, project_name: str) -> list[Dataset]:
        """Lists resources, according to pagination parameters."""
        project = project_accessor.get_by_name(session, project_name)
        stmt = select(self.model).where(self.model.project_id == project.id)
        resources = session.execute(stmt).scalars()
        return [self.schema.from_orm(_) for _ in resources]  # type: ignore

    def serialize_create_schema(
        self, session: Session, resource: DatasetCreate
    ) -> dict:
        """Serializes a DatasetCreate instance into a dict."""
        self.validate_full_name(resource.name)
        serialized_resource = super().serialize_create_schema(session, resource)
        project = self.get_parent_resource(session, resource.name)
        serialized_resource['project_id'] = project.id
        return serialized_resource

    def validate_full_name(self, name: str) -> None:
        """Validates that the name is of form 'project.dataset'."""
        parts = name.split('.')
        if len(parts) != 2:
            raise ValueError(
                f"The dataset name '{name}' is not of the form 'project.dataset'."
            )

    def get_parent_resource(self, session: Session, name: str) -> Project:
        """Returns the `Project` instance associated with the specified dataset name."""
        project_name = name.split('.')[0]
        return project_accessor.get_by_name(session, project_name)


dataset_accessor = AccessorDataset(DBDataset, Dataset)
