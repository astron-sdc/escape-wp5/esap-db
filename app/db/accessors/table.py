"""Provides the dataset accessor object."""
from sqlalchemy.future import select
from sqlalchemy.orm import Session

from ...schemas.dataset import Dataset
from ...schemas.table import Table, TableCreate
from ..dbadmin import DBTable
from .base import AccessorWithNameBase
from .dataset import dataset_accessor


class AccessorTable(AccessorWithNameBase[DBTable, Table, TableCreate]):
    """The Dataset accessor class."""

    resource_type = 'table'

    def list_by_dataset(self, session: Session, dataset_name: str) -> list[Table]:
        """Lists resources, according to pagination parameters."""
        dataset = dataset_accessor.get_by_name(session, dataset_name)
        stmt = select(self.model).where(self.model.dataset_id == dataset.id)
        resources = session.execute(stmt).scalars()
        return [self.schema.from_orm(_) for _ in resources]  # type: ignore

    def serialize_create_schema(self, session: Session, resource: TableCreate) -> dict:
        """Serializes a TableCreate instance into a dict."""
        self.validate_full_name(resource.name)
        serialized_resource = super().serialize_create_schema(session, resource)
        dataset = self.get_parent_resource(session, resource.name)
        serialized_resource['project_id'] = dataset.project_id
        serialized_resource['dataset_id'] = dataset.id
        return serialized_resource

    def validate_full_name(self, name: str) -> None:
        """Validates that the name is of form 'project.dataset.table'."""
        parts = name.split('.')
        if len(parts) != 3:
            raise ValueError(
                f"The table name '{name}' is not of the form 'project.dataset.table'."
            )

    def get_parent_resource(self, session: Session, name: str) -> Dataset:
        """Returns the `Dataset` instance associated with the specified table name."""
        dataset_name = '.'.join(name.split('.')[:2])
        return dataset_accessor.get_by_name(session, dataset_name)


table_accessor = AccessorTable(DBTable, Table)
