"""Provides the project accessor object."""
from ...schemas.project_server import ProjectServer, ProjectServerCreate
from ..dbadmin import DBProjectServer
from .base import AccessorBase


class AccessorProject(
    AccessorBase[DBProjectServer, ProjectServer, ProjectServerCreate]
):
    """The ProjectServer accessor class."""

    resource_type = 'project server'


project_server_accessor = AccessorProject(DBProjectServer, ProjectServer)
