"""Provides the project accessor object."""
from ...schemas.project import Project, ProjectCreate
from ..dbadmin import DBProject
from .base import AccessorWithNameBase


class AccessorProject(AccessorWithNameBase[DBProject, Project, ProjectCreate]):
    """The Project accessor class."""

    resource_type = 'project'


project_accessor = AccessorProject(DBProject, Project)
