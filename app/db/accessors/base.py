"""Provides the base class for accessor objects."""
from typing import Any, Generic, Optional, Type, TypeVar

from fastapi.encoders import jsonable_encoder
from pydantic import BaseModel, SecretStr
from sqlalchemy.exc import IntegrityError
from sqlalchemy.future import select
from sqlalchemy.orm import Session

from ...exceptions import ESAPDBResourceExistsError, ESAPDBResourceNotFoundError
from ..dbadmin.models import Base

DBModelType = TypeVar('DBModelType', bound=Base)
SchemaType = TypeVar('SchemaType', bound=BaseModel)
CreateSchemaType = TypeVar('CreateSchemaType', bound=BaseModel)


class AccessorBase(Generic[DBModelType, SchemaType, CreateSchemaType]):
    """The accessor base class."""

    resource_type = ''

    def __init__(
        self,
        model: Type[DBModelType],
        schema: Type[SchemaType],
    ):
        """Accessor object with default methods to Create, Retrieve, Update, Delete (CRUD).

        Parameters:
          `model`: A SQLAlchemy model class.
          `schema`: A Pydantic model class.
        """
        self.model = model
        self.schema = schema

    def get_by_id(self, session: Session, id: Any) -> SchemaType:
        """Retrieves a resource, as specified by the model primary key."""
        stmt = select(self.model).where(self.model.id == id)  # type: ignore
        resource = session.execute(stmt).scalars().first()
        if resource is None:
            raise ESAPDBResourceNotFoundError(
                f"The {self.resource_type} '{id}' does not exist."
            )

        return self.schema.from_orm(resource)

    def list_(self, session: Session) -> list[SchemaType]:
        """Lists resources, according to pagination parameters."""
        stmt = select(self.model)
        resources = session.execute(stmt).scalars()
        return [self.schema.from_orm(_) for _ in resources]

    def create(self, session: Session, resource: CreateSchemaType) -> SchemaType:
        """Creates a resource."""
        serialized_resource = self.serialize_create_schema(session, resource)
        db_resource = self.model(**serialized_resource)  # type: ignore
        session.add(db_resource)
        try:
            session.flush()
        except IntegrityError:
            msg = f'The {self.resource_type} already exists: {resource}'  # type: ignore  # noqa
            raise ESAPDBResourceExistsError(msg)
        return self.schema.from_orm(db_resource)

    def delete_by_id(self, session: Session, id: Any) -> None:
        """Deletes a resource, as specified by the model primary key."""
        stmt = select(self.model).where(self.model.id == id)  # type: ignore
        db_resource = session.execute(stmt).scalars().first()
        if db_resource is None:
            return
        session.delete(db_resource)
        session.flush()

    def serialize_create_schema(
        self, session: Session, resource: CreateSchemaType
    ) -> dict:
        """Serializes a create schema into a dict."""
        return jsonable_encoder(
            resource, custom_encoder={SecretStr: lambda v: v.get_secret_value()}
        )


class AccessorWithNameBase(AccessorBase[DBModelType, SchemaType, CreateSchemaType]):
    """The accessor base class, for resources that can also be identified by name."""

    def get_by_name(self, session: Session, name: str) -> SchemaType:
        """Retrieves a resource, as specified by their name."""
        self.validate_full_name(name)
        stmt = select(self.model).where(self.model.name == name)  # type: ignore
        resource = session.execute(stmt).scalars().first()
        if resource is not None:
            return self.schema.from_orm(resource)

        msg = f"The {self.resource_type} '{name.split('.')[-1]}' does not exist"
        parent = self.get_parent_resource(session, name)
        if parent is None:
            raise ESAPDBResourceNotFoundError(f'{msg}.')
        parent_type = type(parent).__name__.lower()
        raise ESAPDBResourceNotFoundError(
            f"{msg} in the {parent_type} '{parent.name}'."  # type: ignore
        )

    def create(self, session: Session, resource: CreateSchemaType) -> SchemaType:
        """Creates a resource."""
        try:
            return super().create(session, resource)
        except ESAPDBResourceExistsError:
            msg = f"The {self.resource_type} '{resource.name}' already exists."  # type: ignore  # noqa
            raise ESAPDBResourceExistsError(msg)

    def delete_by_name(self, session: Session, name: str) -> None:
        """Deletes a resource, as specified by their name."""
        self.validate_full_name(name)
        stmt = select(self.model).where(self.model.name == name)  # type: ignore
        db_resource = session.execute(stmt).scalars().first()
        if db_resource is None:
            self.get_parent_resource(session, name)
            return None
        session.delete(db_resource)
        session.flush()

    def validate_full_name(self, name: str) -> None:
        """Validates that the name is correct."""

    def get_parent_resource(self, session: Session, name: str) -> Optional[BaseModel]:
        """Returns the containing resource.

        I.e a project for a dataset and a dataset for a table.
        """
        return None
