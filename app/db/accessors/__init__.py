"""SQLAlchemy accessors for the resources managed by the admin database."""
from .dataset import dataset_accessor as dataset
from .project import project_accessor as project
from .project_server import project_server_accessor as project_server
from .table import table_accessor as table

__all__ = (
    'project_server',
    'project',
    'dataset',
    'table',
)
