"""Factory for the SQLAlchemy engines of the projects, with a caching mechanism."""
import collections
from contextlib import contextmanager
from typing import Iterator

from fastapi import HTTPException, status
from sqlalchemy import create_engine
from sqlalchemy.engine import Connection, Engine
from sqlalchemy.future import select

from ..dbadmin import DBProject, begin_session


class ProjectEngines(collections.abc.Mapping):
    """Cache for the project engines."""

    def __init__(self) -> None:
        """Constructs a new cache for the project engines."""
        self._dict: dict[str, Engine] = {}

    def __getitem__(self, project_name: str) -> Engine:
        """Returns the engine associated to the project."""
        engine = self._dict.get(project_name)
        if engine is None:
            engine = self._create_project_engine(project_name)
            self._dict[project_name] = engine
        return engine

    def __len__(self) -> int:
        """Returns the number of cached engines."""
        return len(self._dict)

    def __iter__(self) -> Iterator[str]:
        """Iterates through the cached engines."""
        return iter(self._dict)

    @staticmethod
    def _create_project_engine(project_name: str) -> Engine:
        """Factory for the project engines."""
        stmt = select(DBProject).where(DBProject.name == project_name)
        with begin_session() as session:
            db_project = session.execute(stmt).scalar_one_or_none()
            if db_project is None:
                raise HTTPException(
                    status.HTTP_404_NOT_FOUND,
                    f"The project '{project_name}' does not exist.",
                )
            return create_engine(db_project.uri, pool_pre_ping=True, future=True)


project_engines = ProjectEngines()


@contextmanager
def begin_connection(project: str) -> Iterator[Connection]:
    """Returns a transaction connection associated with the project database."""
    engine = project_engines[project]
    with engine.begin() as connection:
        yield connection
