"""PostgreSQL operations on the user project database servers."""
import logging

from fastapi import HTTPException, status
from sqlalchemy import create_engine, text
from sqlalchemy.engine import Connection
from sqlalchemy.exc import DBAPIError

from ...helpers import fix_sqlalchemy2_stubs_non_nullable_column

logger = logging.getLogger(__name__)


def create_database(server_uri: str, project_name: str) -> None:
    """Creates a PostgreSQL database."""
    try:
        with create_engine(
            fix_sqlalchemy2_stubs_non_nullable_column(server_uri),
            pool_pre_ping=True,
            future=True,
        ).connect() as conn:
            stmt = text(f'CREATE DATABASE "{project_name}"')
            conn.execution_options(isolation_level='AUTOCOMMIT').execute(stmt)
    except DBAPIError as exc:
        logger.error(str(exc))
        raise HTTPException(
            status_code=status.HTTP_502_BAD_GATEWAY,
            detail='The project database could not be created: '
            f'{type(exc).__name__}: {exc}',
        )


def drop_database(server_uri: str, project_name: str) -> None:
    """Drops a PostgreSQL database."""
    try:
        with create_engine(
            fix_sqlalchemy2_stubs_non_nullable_column(server_uri),
            pool_pre_ping=True,
            future=True,
        ).connect() as conn:
            conn.execution_options(isolation_level='AUTOCOMMIT')
            # remove active connections
            stmt = text(
                f"""
                SELECT pg_terminate_backend (pg_stat_activity.pid)
                FROM pg_stat_activity
                WHERE pg_stat_activity.datname = '{project_name}'"""
            )
            conn.execute(stmt)
            stmt = text(f'DROP DATABASE IF EXISTS "{project_name}"')
            conn.execute(stmt)
    except DBAPIError as exc:
        logger.error(str(exc))
        raise HTTPException(
            status_code=status.HTTP_502_BAD_GATEWAY,
            detail='The project database could not be removed: '
            f'{type(exc).__name__}: {exc}',
        )


def create_schema(connection: Connection, dataset_name: str) -> None:
    """Creates a schema in the database specified by the connection."""
    _, schema = dataset_name.split('.')
    stmt = text(f'CREATE SCHEMA {schema}')
    connection.execute(stmt)


def drop_schema(connection: Connection, dataset_name: str) -> None:
    """Drops a schema in the database specified by the connection."""
    _, schema = dataset_name.split('.')
    stmt = text(f'DROP SCHEMA IF EXISTS {schema} RESTRICT')
    connection.execute(stmt)


def drop_table(connection: Connection, table_name: str) -> None:
    """Drops a table from a schema in the database specified by the connection."""
    _, table = table_name.split('.', maxsplit=1)
    stmt = text(f'DROP TABLE IF EXISTS {table} CASCADE')
    connection.execute(stmt)


def select_table_column_names(
    connection: Connection, dataset: str, table: str
) -> list[str]:
    """Returns the column names of a table."""
    stmt = text(
        f"""
        SELECT column_name
        FROM information_schema.columns
        WHERE table_schema = '{dataset}' AND table_name = '{table}'
        """
    )
    result = connection.execute(stmt)
    return result.scalars().all()
