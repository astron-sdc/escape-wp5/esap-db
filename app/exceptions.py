"""This modules defines the exceptions used by ESAP-DB.

These exceptions, if not caught, are handled by the FastAPI error handlers
and translated into HTTP errors.
"""
from __future__ import annotations

from fastapi import Request
from fastapi.responses import JSONResponse


class ESAPDBError(Exception):
    """The base class for ESAP-DB exceptions."""

    status_code = 500

    @classmethod
    async def error_handler(cls, request: Request, exc: ESAPDBError) -> JSONResponse:
        """The FastAPI error handler associated with this exception."""
        return JSONResponse(
            status_code=cls.status_code,
            content={'status': cls.status_code, 'detail': str(exc)},
        )


class ESAPDBValidationError(ESAPDBError, ValueError):
    """When a validation specific to ESAP-DB fails."""

    status_code = 400


class ESAPDBResourceNotFoundError(ESAPDBError):
    """When a resource is expected to exist and does not."""

    status_code = 404


class ESAPDBResourceExistsError(ESAPDBError):
    """When a resource is not expected to already exists."""

    status_code = 409


class ESAPDBNotImplementedError(ESAPDBError):
    """When a request is not yet implemented."""

    status_code = 501
