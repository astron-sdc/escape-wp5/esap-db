# ESAP-DB

ESAP-DB is an ESAP component providing managed database services.


## Installation

After cloning the ESAP-DB repository:

```bash
git clone git@git.astron.nl:astron-sdc/esap-db.git
cd esap-db
```

add this .env file in the `host-files` directory under the current directory (`esap-db`):

```
DOMAIN=localhost
DBADMIN_USER=postgres
DBADMIN_PASSWORD=postgres
DBPROJECT_USER=postgres
DBPROJECT_PASSWORD=postgres
PGADMIN_DEFAULT_EMAIL=pgadmin@pgadmin.org
PGADMIN_DEFAULT_PASSWORD=admin
TRAEFIK_PUBLIC_NETWORK_IS_EXTERNAL=false
```
If you run esap-db behind a reverse proxy, you can also configue a root path by adding:
```
ROOTPATH=/esap-db
```

To check that ESAP-DB is working locally:

```bash
host-files/run-tests.sh
```
Installation is successful if the tests pass.


## Using ESAP-DB

In order to use ESAP-DB locally, you can launch the required services in a ternminal:

```bash
host-files/run.sh
```

In a browser, the ESAP-DB apis can be tested at the url `http://localhost:8001/docs` or `http://localhost:8001/redoc`.

The PostgreSQL administration platform `pgAdmin` can be accessed at the url `http://localhost:5050`. It can be used to view the dbadmin and dbproject1 databases.

## Storing an ESAP-API request into a table.

Let's create a project:
```bash
curl -XPOST \
  'http://localhost:8001/api/v0/projects' \
  -H 'Content-Type: application/json' \
  -d '{
    "name": "my_project",
    "description": "My first project 😀"
  }'
```

In this project, let's create a dataset, i.e., a group of tables:
```bash
curl -XPOST \
  'http://localhost:8001/api/v0/projects/my_project/datasets' \
  -H 'Content-Type: application/json' \
  -d '{
    "name": "my_project.my_dataset",
    "description": "My first dataset 😃"
  }'
```

Then, we can store the result of an ESAP-API query into a table that belongs to the dataset that we have just created.
```bash
curl -X 'POST' \
  'http://localhost:8001/api/v0/projects/my_project/esap-gateway-operations' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
    "name": "my_project.my_dataset.apertif_table",
    "description": "Apertif cone search.",
    "query": {
      "level": "raw",
      "collection": "imaging",
      "ra": 342.16,
      "dec": 33.94,
      "fov": 10,
      "archive_uri": "apertif"
    }
  }'
```

The new table `apertif_table` can be exported as JSON:
```bash
curl -X 'GET' \
  'http://localhost:8001/api/v0/projects/my_project/datasets/my_dataset/tables/apertif_table/content' \
  -H 'accept: application/json'
```

## Contributing

For developer access to this repository, please send a message on the [ESAP channel on Rocket Chat](https://chat.escape2020.de/channel/esap).
