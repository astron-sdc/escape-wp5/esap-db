"""Table project_servers.

Revision ID: 5dd23cffa253
Revises: 6e7e210ad4b8
Create Date: 2021-07-06 16:33:36.478623

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = '5dd23cffa253'
down_revision = '6e7e210ad4b8'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table(
        'project_servers',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('uri', sa.String(), nullable=False),
        sa.Column('max_size', sa.BigInteger(), nullable=False),
        sa.Column('available_size', sa.BigInteger(), nullable=False),
        sa.PrimaryKeyConstraint('id'),
        sa.UniqueConstraint('uri'),
    )
    op.create_index(
        op.f('ix_project_servers_id'), 'project_servers', ['id'], unique=False
    )
    op.create_table(
        'projects',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('project_server_id', sa.Integer(), nullable=True),
        sa.Column('name', sa.String(length=256), nullable=False),
        sa.Column('description', sa.String(), nullable=False),
        sa.Column('uri', sa.String(), nullable=False),
        sa.Column('max_size', sa.BigInteger(), nullable=False),
        sa.ForeignKeyConstraint(
            ['project_server_id'],
            ['project_servers.id'],
        ),
        sa.PrimaryKeyConstraint('id'),
    )
    op.create_index(op.f('ix_projects_id'), 'projects', ['id'], unique=False)
    op.create_index(op.f('ix_projects_name'), 'projects', ['name'], unique=False)
    op.add_column('users', sa.Column('is_superuser', sa.Boolean(), nullable=False))
    op.alter_column('users', 'first_name', existing_type=sa.VARCHAR(), nullable=False)
    op.alter_column('users', 'last_name', existing_type=sa.VARCHAR(), nullable=False)
    op.alter_column('users', 'email', existing_type=sa.VARCHAR(), nullable=False)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('users', 'email', existing_type=sa.VARCHAR(), nullable=True)
    op.alter_column('users', 'last_name', existing_type=sa.VARCHAR(), nullable=True)
    op.alter_column('users', 'first_name', existing_type=sa.VARCHAR(), nullable=True)
    op.drop_column('users', 'is_superuser')
    op.drop_index(op.f('ix_projects_name'), table_name='projects')
    op.drop_index(op.f('ix_projects_id'), table_name='projects')
    op.drop_table('projects')
    op.drop_index(op.f('ix_project_servers_id'), table_name='project_servers')
    op.drop_table('project_servers')
    # ### end Alembic commands ###
