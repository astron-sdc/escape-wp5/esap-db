#! /usr/bin/env bash

# Let the DB start
python /code/scripts/wait_for_dbadmin.py

# Run migrations
alembic upgrade head

# Create initial data in DB
python /code/scripts/initialize_dbadmin.py
