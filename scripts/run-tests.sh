#! /usr/bin/env bash
set -ex

# Let the DB start
python /code/scripts/wait_for_initialized_dbadmin.py

pytest --cov=app --cov-report=term-missing "${@}"
