"""This script initializes the admin database."""
import logging

from app import db
from app.config import settings
from app.db import vo_project
from app.db.dbadmin import begin_session
from app.exceptions import ESAPDBResourceExistsError
from app.schemas import ProjectServerCreate

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def initialize_dbadmin() -> None:
    """Initializes the admin database."""
    for server_name in settings.DBPROJECT_SERVERS:
        server = ProjectServerCreate(
            uri=f'postgresql://{settings.DBPROJECT_USER}:{settings.DBPROJECT_PASSWORD}@{server_name}',  # noqa
            max_size=2 ** 50,  # 1 PiB
        )
        try:
            with begin_session() as session:
                db.project_server.create(session, server)
            logger.info(f'Added project server: {server_name}')
        except ESAPDBResourceExistsError:
            logger.info(f'The project server is already present: {server_name}')

    for project in vo_project.fetch_all():
        try:
            with begin_session() as session:
                db.project.create(session, project)
            logger.info(f'Added VO TAP service: {project.name}')
        except ESAPDBResourceExistsError:
            logger.info(f'The project is already present: {project.name}')


def main() -> None:
    """Logs and initializes the admin database."""
    logger.info('Creating initial data')
    initialize_dbadmin()
    logger.info('Initial data created')


if __name__ == '__main__':
    main()
