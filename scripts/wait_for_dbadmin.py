"""This script waits until a connection is established to the admin database."""
import logging

from sqlalchemy import select
from tenacity import after_log, before_log, retry, stop_after_attempt, wait_fixed

from app.db.dbadmin import begin_session

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

max_tries = 60 * 5  # 5 minutes
wait_seconds = 1


@retry(
    stop=stop_after_attempt(max_tries),
    wait=wait_fixed(wait_seconds),
    before=before_log(logger, logging.INFO),
    after=after_log(logger, logging.WARN),
)
def execute_command() -> None:
    """Attempts to execute a command in the admin database."""
    with begin_session() as session:
        try:
            # Try to create session to check if DB is awake
            session.execute(select(1))
        except Exception as e:
            logger.error(e)
            raise e


def main() -> None:
    """Logs and connects to the admin database."""
    logger.info('Initializing service')
    execute_command()
    logger.info('Service finished initializing')


if __name__ == '__main__':
    main()
