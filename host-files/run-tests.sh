#! /usr/bin/env bash

# Exit in case of error
set -e

export ESAP_DB_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

BUILD_AND_RUN=true INSTALL_DEV=true ${ESAP_DB_PATH}/run.sh -d
docker-compose -f ${ESAP_DB_PATH}/docker-stack.yml exec -T esap-db bash /code/scripts/run-tests.sh "$@"
docker-compose -f ${ESAP_DB_PATH}/docker-stack.yml down -v --remove-orphans
