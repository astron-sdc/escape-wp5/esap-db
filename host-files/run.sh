#! /usr/bin/env bash

# Exit in case of error
set -e

export BUILD_AND_RUN=${BUILD_AND_RUN:-false}

if [ $BUILD_AND_RUN = true ]
then
  ${ESAP_DB_PATH}/build.sh
fi

export ESAP_DB_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

docker-compose -f ${ESAP_DB_PATH}/docker-stack.yml up "$@"
