from typing import Iterator

import pytest
from fastapi.testclient import TestClient

from app.main import app


@pytest.fixture(scope='session')
def client() -> Iterator[TestClient]:
    with TestClient(app) as c:
        yield c
