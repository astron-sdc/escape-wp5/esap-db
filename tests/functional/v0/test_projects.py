from fastapi.testclient import TestClient

from app.config import settings
from app.helpers import uid
from app.schemas import Project

from .helpers import staged_project


def test_create_project(client: TestClient) -> None:
    project_in = {
        'name': uid('project'),
        'description': uid(),
        'max_size': 1000,
    }
    with staged_project(client, project_in) as project:
        pass
    assert project.name == project_in['name']
    assert project.description == project_in['description']
    assert project.max_size == project_in['max_size']
    assert project.id is not None
    assert project.project_server_id is not None


def test_create_project_duplicate_failure(client: TestClient, project: Project) -> None:
    project_in = {'name': project.name}
    response = client.post(f'{settings.API_V0_STR}/projects', json=project_in)
    assert response.status_code == 409


def test_create_project_too_big_failure(client: TestClient) -> None:
    max_size = 2 ** 50
    project = {
        'name': uid('project'),
        'max_size': max_size + 1,
    }
    response = client.post(f'{settings.API_V0_STR}/projects', json=project)
    assert response.status_code == 507


def test_get_project_failure(client: TestClient) -> None:
    response = client.get(f'{settings.API_V0_STR}/projects/{uid("project")}')
    assert response.status_code == 404


def test_get_project(client: TestClient, project: Project) -> None:
    response = client.get(f'{settings.API_V0_STR}/projects/{project.name}')
    assert response.status_code == 200
    project_get = response.json()
    assert project_get['name'] == project.name
    assert project_get['description'] == project.description
    assert project_get['max_size'] == project.max_size
    assert project_get['id'] == project.id
    assert project_get['project_server_id'] == project.project_server_id
    assert project_get['uri'].startswith('postgresql://********:********@')
    assert project_get['type'] == 'user'


def test_list_projects(client: TestClient, project: Project) -> None:
    response = client.get(f'{settings.API_V0_STR}/projects')
    assert response.status_code == 200
    serialized_projects = response.json()
    assert project.name in (_['name'] for _ in serialized_projects)


def test_delete_project_success(client: TestClient) -> None:
    project = {'name': uid('project')}
    response = client.post(f'{settings.API_V0_STR}/projects', json=project)
    assert response.status_code == 200
    response = client.delete(f'{settings.API_V0_STR}/projects/{project["name"]}')
    assert response.status_code == 204
    response = client.get(f'{settings.API_V0_STR}/projects/{project["name"]}')
    assert response.status_code == 404


def test_delete_project_failure(client: TestClient) -> None:
    project = {'name': uid('project')}
    response = client.post(f'{settings.API_V0_STR}/projects', json=project)
    assert response.status_code == 200
    dataset = {'name': uid('dataset')}
    response = client.post(
        f'{settings.API_V0_STR}/projects/{project["name"]}/datasets',
        json=dataset,
    )
    assert response.status_code == 200
    response = client.delete(f'{settings.API_V0_STR}/projects/{project["name"]}')
    assert response.status_code == 409
    response = client.get(f'{settings.API_V0_STR}/projects/{project["name"]}')
    assert response.status_code == 200
