from fastapi.testclient import TestClient

from app.config import settings
from app.helpers import uid
from app.schemas import Dataset, Project

from .helpers import staged_dataset, staged_table


def test_create_dataset_success(client: TestClient, project: Project) -> None:
    dataset = {
        'name': uid('dataset'),
        'description': uid() + '🍮🐉α𝒷𝓒ᗪｅŦ🎯☹',
    }
    with staged_dataset(client, project, dataset) as dataset_post:
        pass
    assert dataset_post.name == f'{project.name}.{dataset["name"]}'
    assert dataset_post.description == dataset['description']
    assert dataset_post.id is not None
    assert dataset_post.project_id == project.id


def test_create_dataset_unknown_project_failure(client: TestClient) -> None:
    dataset = {'name': uid('dataset')}
    response = client.post(
        f'{settings.API_V0_STR}/projects/UNKNOWN/datasets', json=dataset
    )
    assert response.status_code == 404


def test_create_dataset_duplicate_failure(client: TestClient, dataset: Dataset) -> None:
    project_name, _ = dataset.name.split('.')
    dataset_in = {'name': dataset.name}
    api = f'{settings.API_V0_STR}/projects/{project_name}/datasets'
    response = client.post(api, json=dataset_in)
    assert response.status_code == 409


def test_create_dataset_invalid_path(client: TestClient, project: Project) -> None:
    dataset = {'name': 'project.dataset'}
    api = f'{settings.API_V0_STR}/projects/{project.name}/datasets'
    response = client.post(api, json=dataset)
    assert response.status_code == 400
    msg = response.json()['detail']
    assert (
        msg == f'The project specified in the path ({project.name}) differs '
        'from that specified in the request body (project).'
    )


def test_get_dataset_success(client: TestClient, dataset: Dataset) -> None:
    project_name, dataset_name = dataset.name.split('.')
    response = client.get(
        f'{settings.API_V0_STR}/projects/{project_name}/datasets/{dataset_name}'
    )
    assert response.status_code == 200
    assert response.json() == dataset.dict()


def test_get_dataset_failure1(client: TestClient) -> None:
    response = client.get(
        f'{settings.API_V0_STR}/projects/UNKNOWN_PROJECT/datasets/UNKNOWN_DATASET'
    )
    assert response.status_code == 404
    detail = response.json()['detail']
    assert detail == "The project 'UNKNOWN_PROJECT' does not exist."


def test_get_dataset_failure2(client: TestClient, project: Project) -> None:
    response = client.get(
        f'{settings.API_V0_STR}/projects/{project.name}/datasets/UNKNOWN_DATASET'
    )
    assert response.status_code == 404
    detail = response.json()['detail']
    assert (
        detail
        == f"The dataset 'UNKNOWN_DATASET' does not exist in the project '{project.name}'."
    )


def test_list_datasets(client: TestClient, project: Project, dataset: Dataset) -> None:
    response = client.get(f'{settings.API_V0_STR}/projects/{project.name}/datasets')
    assert response.status_code == 200
    serialized_datasets = response.json()
    assert dataset.name in (_['name'] for _ in serialized_datasets)


def test_delete_dataset_success(client: TestClient, project: Project) -> None:
    with staged_dataset(client, project) as dataset:
        project_name, dataset_name = dataset.name.split('.')
    response = client.get(
        f'{settings.API_V0_STR}/projects/{project_name}/datasets/{dataset_name}'
    )
    assert response.status_code == 404


def test_delete_non_empty_failure(client: TestClient, dataset: Dataset) -> None:
    project_name, dataset_name = dataset.name.split('.')
    with staged_table(client, dataset):
        response = client.delete(
            f'{settings.API_V0_STR}/projects/{project_name}/datasets/{dataset_name}'
        )
        assert response.status_code == 409
    response = client.get(
        f'{settings.API_V0_STR}/projects/{project_name}/datasets/{dataset_name}'
    )
    assert response.status_code == 200


def test_delete_project_not_found_failure(client: TestClient) -> None:
    project_name = uid()
    response = client.delete(
        f'{settings.API_V0_STR}/projects/{project_name}/datasets/dataset',
    )
    assert response.status_code == 404
    assert response.json()['detail'] == f"The project '{project_name}' does not exist."
