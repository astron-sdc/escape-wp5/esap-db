from typing import Iterator

import pytest
import responses
from fastapi.testclient import TestClient

from app.schemas import Dataset, Project

from .helpers import staged_dataset, staged_project


@pytest.fixture(scope='package')
def project(client: TestClient) -> Iterator[Project]:
    with staged_project(client) as project:
        yield project


@pytest.fixture(scope='package')
def dataset(client: TestClient, project: Project) -> Iterator[Dataset]:
    with staged_dataset(client, project) as dataset:
        yield dataset


@pytest.fixture
def mocked_responses() -> Iterator[responses.RequestsMock]:
    with responses.RequestsMock() as rsps:
        yield rsps
