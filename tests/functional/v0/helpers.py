from contextlib import contextmanager
from pathlib import Path
from typing import Iterator, Optional

from fastapi.testclient import TestClient

from app.config import settings
from app.helpers import uid
from app.schemas import Dataset, Project, Table

DATA_PATH = Path(__file__).parents[1] / 'data'


@contextmanager
def staged_project(
    client: TestClient, project: Optional[dict] = None
) -> Iterator[Project]:
    """Creates and removes a project in the project database."""
    if project is None:
        project = {
            'name': uid('project'),
            'description': uid() + '♩♪♫♬♭♮♯🎼',
        }
    response = client.post(f'{settings.API_V0_STR}/projects', json=project)
    assert response.status_code == 200
    project_out = Project(**response.json())

    try:
        yield project_out
    finally:
        response = client.delete(f'{settings.API_V0_STR}/projects/{project_out.name}')
        assert response.status_code == 204


@contextmanager
def staged_dataset(
    client: TestClient, project: Project, dataset: Optional[dict] = None
) -> Iterator[Dataset]:
    """Creates and removes a dataset in the project database."""
    if dataset is None:
        dataset = {
            'name': uid('dataset'),
            'description': uid() + '🍮🐉α𝒷𝓒ᗪｅŦ🎯☹',
        }
    response = client.post(
        f'{settings.API_V0_STR}/projects/{project.name}/datasets', json=dataset
    )
    assert response.status_code == 200
    dataset_out = Dataset(**response.json())
    project_name, dataset_name = dataset_out.name.split('.')

    try:
        yield dataset_out
    finally:
        response = client.delete(
            f'{settings.API_V0_STR}/projects/{project_name}/datasets/{dataset_name}'
        )
        assert response.status_code == 204


@contextmanager
def staged_table(
    client: TestClient, dataset: Dataset, table: Optional[dict] = None
) -> Iterator[Table]:
    """Creates and removes a table in a dataset."""
    if table is None:
        table = {
            'name': uid('table'),
            'description': uid() + 'ෛญޘ࿄㐦😀',
            'content': {'x': [1, 2, 3], 'y': ['a', 'b', 'c']},
        }
    project_name, dataset_name = dataset.name.split('.')
    response = client.post(
        f'{settings.API_V0_STR}/projects/{project_name}/datasets/{dataset_name}/tables',
        json=table,
    )
    assert response.status_code == 200
    table_out = Table(**response.json())

    try:
        yield table_out
    finally:
        table_name = table_out.name.split('.')[-1]
        response = client.delete(
            f'{settings.API_V0_STR}/projects/{project_name}/datasets/{dataset_name}/tables/{table_name}'
        )
        assert response.status_code == 204
