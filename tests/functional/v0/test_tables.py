import base64
import pickle

import pandas as pd
import responses
from fastapi.testclient import TestClient

from app.config import settings
from app.helpers import uid
from app.schemas import Dataset, Project, Table

from .helpers import DATA_PATH, staged_dataset, staged_table


def test_create_table_mapping_success(client: TestClient, dataset: Dataset) -> None:
    payload = {
        'name': uid('table'),
        'description': uid() + 'ෛญޘ࿄㐦😀',
        'content': {'x': [1, 2, 3, 4], 'y': ['a', 'b', 'c', 'd']},
    }
    with staged_table(client, dataset, payload) as table:
        project_name, dataset_name, table_name = table.name.split('.')
        api = f'{settings.API_V0_STR}/projects/{project_name}/datasets/{dataset_name}/tables/{table_name}/content'
        response = client.get(api)
    assert response.status_code == 200
    actual = pd.DataFrame(response.json())
    expected = pd.DataFrame(payload['content'])
    assert actual.equals(expected)
    assert table.name == f'{dataset.name}.{payload["name"]}'
    assert table.description == payload['description']


def test_create_table_dataframe_success(client: TestClient, dataset: Dataset) -> None:
    expected = pd.DataFrame({'x': [1, 2, 3, 4], 'y': ['a', 'b', 'c', 'd']})
    payload = {
        'name': uid('table'),
        'description': uid() + 'ෛญޘ࿄㐦😀',
        'content': base64.b64encode(pickle.dumps(expected)).decode('ascii'),
    }
    with staged_table(client, dataset, payload) as table:
        project_name, dataset_name, table_name = table.name.split('.')
        api = f'{settings.API_V0_STR}/projects/{project_name}/datasets/{dataset_name}/tables/{table_name}/content'
        response = client.get(api)
    assert response.status_code == 200
    actual = pd.DataFrame(response.json())
    assert actual.equals(expected)
    assert table.name == f'{dataset.name}.{payload["name"]}'
    assert table.description == payload['description']


def test_create_table_csv_success(
    mocked_responses: responses.RequestsMock, client: TestClient, dataset: Dataset
) -> None:
    path = DATA_PATH / 'fiphfp-taux-emploi_national.csv'
    raw = (DATA_PATH / 'fiphfp-taux-emploi_national.headers').read_text()
    headers = dict(_.split(': ') for _ in raw.strip().split('\n'))
    url = 'https://www.data.gouv.fr/fr/datasets/r/d1674cbd-9703-4e40-8fde-e5395649eaf4'
    mocked_responses.add(
        responses.GET, url, body=path.read_text(), headers=headers, status=200
    )
    expected = pd.read_csv(path, sep=';')
    payload = {'description': uid() + 'ෛญޘ࿄㐦😀', 'path': url, 'params': {'sep': ';'}}
    with staged_table(client, dataset, payload) as table:
        project_name, dataset_name, table_name = table.name.split('.')
        api = f'{settings.API_V0_STR}/projects/{project_name}/datasets/{dataset_name}/tables/{table_name}/content'
        response = client.get(api)
    assert response.status_code == 200
    actual = pd.DataFrame(response.json())
    assert actual.equals(expected)
    assert table.name == f'{dataset.name}.fiphfp_taux_emploi_national'
    assert table.description == payload['description']


def test_create_table_query_success(
    client: TestClient, project: Project, dataset: Dataset
) -> None:
    payload1 = {
        'name': uid('table1'),
        'content': {'x': 5 * ['vegetable'], 'y': list('🥑🌽🥒🍆🥦')},
    }
    payload2 = {
        'name': uid('table2'),
        'description': 'Union of two tables',
        'content': {'x': 6 * ['fruit'], 'y': list('🍓🥝🍇🍐🍏🍍')},
    }
    with staged_dataset(client, project) as dataset1, staged_table(
        client, dataset1, payload1
    ) as table1, staged_dataset(client, project) as dataset2, staged_table(
        client, dataset2, payload2
    ) as table2:
        _, dataset1_name, table1_name = table1.name.split('.')
        _, dataset2_name, table2_name = table2.name.split('.')
        payload3 = {
            'name': f'{dataset.name}.{uid("table3")}',
            'description': 'union table',
            'query': f'SELECT * FROM {dataset1_name}.{table1_name} UNION SELECT * FROM {dataset2_name}.{table2_name} ORDER BY x, y',
        }
        with staged_table(client, dataset, payload3) as table3:
            project_name, dataset_name, table_name = table3.name.split('.')
            api = f'{settings.API_V0_STR}/projects/{project_name}/datasets/{dataset_name}/tables/{table_name}/content'
            response = client.get(api)
    assert response.status_code == 200
    actual = pd.DataFrame(response.json())
    # FIXME the order is wrong, should investigate collation
    actual = actual.sort_values(['x', 'y']).reset_index(drop=True)
    df1 = pd.DataFrame(payload1['content'])
    df2 = pd.DataFrame(payload2['content'])
    expected = pd.concat([df1, df2]).sort_values(['x', 'y']).reset_index(drop=True)
    assert actual.equals(expected)
    assert table3.name == payload3['name']
    assert table3.description == payload3['description']


def test_create_table_not_found(client: TestClient, project: Project) -> None:
    payload = {
        'name': uid('table'),
        'description': uid() + 'ෛญޘ࿄㐦😀',
        'content': {'x': [1, 2, 3, 4], 'y': ['a', 'b', 'c', 'd']},
    }
    url = f'{settings.API_V0_STR}/projects/UNKNOWN_PROJECT/datasets/UNKNOWN_DATASET/tables'
    response = client.post(url, json=payload)
    assert response.status_code == 404
    detail = response.json()['detail']
    assert detail == "The project 'UNKNOWN_PROJECT' does not exist."

    response = client.post(
        f'{settings.API_V0_STR}/projects/{project.name}/datasets/UNKNOWN_DATASET/tables',
        json=payload,
    )
    assert response.status_code == 404
    detail = response.json()['detail']
    assert (
        detail
        == f"The dataset 'UNKNOWN_DATASET' does not exist in the project '{project.name}'."
    )


def test_create_table_already_exists_failure(
    client: TestClient, dataset: Dataset
) -> None:
    project_name, dataset_name = dataset.name.split('.')
    with staged_table(client, dataset) as table:
        payload = {'name': table.name, 'content': {'x': [1], 'y': ['a']}}
        response = client.post(
            f'{settings.API_V0_STR}/projects/{project_name}/datasets/{dataset_name}/tables',
            json=payload,
        )
    assert response.status_code == 409


def test_create_table_invalid_path1(client: TestClient, dataset: Dataset) -> None:
    project_name, _ = dataset.name.split('.')
    payload = {
        'name': 'project.dataset.table',
        'content': {'x': [1], 'y': ['a']},
    }
    response = client.post(
        f'{settings.API_V0_STR}/projects/{project_name}/datasets/dataset/tables',
        json=payload,
    )
    assert response.status_code == 400
    msg = response.json()['detail']
    assert (
        msg == f'The project specified in the path ({project_name}) differs '
        'from that specified in the request body (project).'
    )


def test_create_table_invalid_path2(client: TestClient, dataset: Dataset) -> None:
    project_name, _ = dataset.name.split('.')
    payload = {
        'name': f'{project_name}.dataset1.table',
        'content': {'x': [1], 'y': ['a']},
    }
    response = client.post(
        f'{settings.API_V0_STR}/projects/{project_name}/datasets/dataset2/tables',
        json=payload,
    )
    assert response.status_code == 400
    msg = response.json()['detail']
    assert (
        msg == 'The dataset specified in the path (dataset2) differs '
        'from that specified in the request body (dataset1).'
    )


def test_get_table_success(client: TestClient, dataset: Dataset) -> None:
    with staged_table(client, dataset) as table:
        project_name, dataset_name, table_name = table.name.split('.')
        response = client.get(
            f'{settings.API_V0_STR}/projects/{project_name}/datasets/{dataset_name}/tables/{table_name}'
        )
    assert response.status_code == 200
    table_get = Table(**response.json())
    assert table_get == table


def test_get_table_failure1(client: TestClient) -> None:
    response = client.get(
        f'{settings.API_V0_STR}/projects/UNKNOWN_PROJECT/datasets/UNKNOWN_DATASET/tables/UNKNOWN_TABLE'
    )
    assert response.status_code == 404
    detail = response.json()['detail']
    assert detail == "The project 'UNKNOWN_PROJECT' does not exist."


def test_get_table_failure2(client: TestClient, project: Project) -> None:
    response = client.get(
        f'{settings.API_V0_STR}/projects/{project.name}/datasets/UNKNOWN_DATASET/tables/UNKNOWN_TABLE'
    )
    assert response.status_code == 404
    detail = response.json()['detail']
    assert (
        detail
        == f"The dataset 'UNKNOWN_DATASET' does not exist in the project '{project.name}'."
    )


def test_get_table_failure3(client: TestClient, dataset: Dataset) -> None:
    project_name, dataset_name = dataset.name.split('.')
    response = client.get(
        f'{settings.API_V0_STR}/projects/{project_name}/datasets/{dataset_name}/tables/UNKNOWN_TABLE'
    )
    assert response.status_code == 404
    detail = response.json()['detail']
    assert (
        detail
        == f"The table 'UNKNOWN_TABLE' does not exist in the dataset '{dataset.name}'."
    )


def test_list_tables(client: TestClient, dataset: Dataset) -> None:
    with staged_table(client, dataset) as table:
        project_name, dataset_name = dataset.name.split('.')
        response = client.get(
            f'{settings.API_V0_STR}/projects/{project_name}/datasets/{dataset_name}/tables'
        )
    assert response.status_code == 200
    serialized_tables = response.json()
    assert table.name in (_['name'] for _ in serialized_tables)


def test_get_table_content(client: TestClient, dataset: Dataset) -> None:
    payload = {
        'name': uid('table'),
        'content': {'x': [1, 2], 'y': ['a', 'b']},
    }
    with staged_table(client, dataset, payload) as table:
        project_name, dataset_name, table_name = table.name.split('.')
        response = client.get(
            f'{settings.API_V0_STR}/projects/{project_name}/datasets/{dataset_name}/tables/{table_name}/content'
        )
    assert response.status_code == 200
    actual = response.json()
    assert actual == [{'x': 1, 'y': 'a'}, {'x': 2, 'y': 'b'}]


def test_get_table_column_names(client: TestClient, dataset: Dataset) -> None:
    payload = {
        'name': uid('table'),
        'content': {'x': [1], 'y': [2.0]},
    }
    with staged_table(client, dataset, payload) as table:
        project_name, dataset_name, table_name = table.name.split('.')
        response = client.get(
            f'{settings.API_V0_STR}/projects/{project_name}/datasets/{dataset_name}/tables/{table_name}/column-names'
        )
    assert response.status_code == 200
    actual = set(response.json())
    assert actual == {'x', 'y'}


def test_delete_table_success(client: TestClient, dataset: Dataset) -> None:
    with staged_table(client, dataset) as table:
        project_name, dataset_name, table_name = table.name.split('.')
    response = client.get(
        f'{settings.API_V0_STR}/projects/{project_name}/datasets/{dataset_name}/tables/{table_name}'
    )
    assert response.status_code == 404


def test_delete_table_non_existent_success(
    client: TestClient, dataset: Dataset
) -> None:
    project_name, dataset_name = dataset.name.split('.')
    response = client.delete(
        f'{settings.API_V0_STR}/projects/{project_name}/datasets/{dataset_name}/tables/{uid("table")}'
    )
    assert response.status_code == 204


def test_delete_table_not_found_project_failure(client: TestClient) -> None:
    project_name = uid('project')
    response = client.delete(
        f'{settings.API_V0_STR}/projects/{project_name}/datasets/dataset/tables/table',
    )
    assert response.status_code == 404
    assert response.json()['detail'] == f"The project '{project_name}' does not exist."


def test_delete_table_not_found_dataset_failure(
    client: TestClient, project: Project
) -> None:
    dataset_name = uid('dataset')
    response = client.delete(
        f'{settings.API_V0_STR}/projects/{project.name}/datasets/{dataset_name}/tables/table',
    )
    assert response.status_code == 404
    assert (
        response.json()['detail']
        == f"The dataset '{dataset_name}' does not exist in the project '{project.name}'."
    )
