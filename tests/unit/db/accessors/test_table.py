import re

import pytest
from sqlalchemy.orm import Session

from app import db
from app.exceptions import ESAPDBResourceExistsError, ESAPDBResourceNotFoundError
from app.schemas import Dataset

from .helpers import fake_table


def test_create(session: Session, dataset: Dataset) -> None:
    table_create = fake_table(dataset.name)
    table = db.table.create(session, table_create)
    assert table.id is not None
    assert table.project_id == dataset.project_id
    assert table.dataset_id == dataset.id
    for field in table_create.__fields__:
        assert getattr(table_create, field) == getattr(table, field)


def test_create_duplicate_failure(session: Session, dataset: Dataset) -> None:
    table_create = fake_table(dataset.name)
    db.table.create(session, table_create)
    msg = f"The table '{table_create.name}' already exists."
    with pytest.raises(ESAPDBResourceExistsError, match=re.escape(msg)):
        db.table.create(session, table_create)


def test_list(session: Session, dataset: Dataset) -> None:
    table = fake_table(dataset.name)
    db.table.create(session, table)
    tables = db.table.list_by_dataset(session, dataset.name)
    assert table.name in (_.name for _ in tables)


def test_get_by_name(session: Session, dataset: Dataset) -> None:
    table = db.table.create(session, fake_table(dataset.name))
    table_get = db.table.get_by_name(session, table.name)
    assert table == table_get


def test_get_by_name_failure1(session: Session) -> None:
    msg = "The table name 'my_table' is not of the form 'project.dataset.table'."
    with pytest.raises(ValueError, match=re.escape(msg)):
        db.table.get_by_name(session, 'my_table')


def test_get_by_name_failure2(session: Session) -> None:
    msg = "The table name 'my1.my2' is not of the form 'project.dataset.table'."
    with pytest.raises(ValueError, match=re.escape(msg)):
        db.table.get_by_name(session, 'my1.my2')


def test_delete_by_name(session: Session, dataset: Dataset) -> None:
    table = db.table.create(session, fake_table(dataset.name))
    db.table.delete_by_name(session, table.name)
    with pytest.raises(ESAPDBResourceNotFoundError):
        db.table.get_by_name(session, table.name)
    db.table.delete_by_name(session, table.name)


def test_delete_by_name_failure1(session: Session) -> None:
    msg = "The table name 'my_table' is not of the form 'project.dataset.table'."
    with pytest.raises(ValueError, match=re.escape(msg)):
        db.table.delete_by_name(session, 'my_table')


def test_delete_by_name_failure2(session: Session) -> None:
    msg = "The table name 'my1.my2' is not of the form 'project.dataset.table'."
    with pytest.raises(ValueError, match=re.escape(msg)):
        db.table.delete_by_name(session, 'my1.my2')
