import random
import re

import pytest
from sqlalchemy.orm import Session

from app import db
from app.exceptions import ESAPDBResourceExistsError, ESAPDBResourceNotFoundError
from app.schemas import ProjectServer

from .helpers import fake_project_server


def test_create(session: Session) -> None:
    project_server_create = fake_project_server()
    project_server = db.project_server.create(session, project_server_create)
    assert project_server.id is not None
    for field in project_server_create.__fields__:
        assert getattr(project_server_create, field) == getattr(project_server, field)


def test_create_duplicate_failure(session: Session) -> None:
    project_server_create = fake_project_server()
    db.project_server.create(session, project_server_create)
    msg = f'The project server already exists: {project_server_create}'
    with pytest.raises(ESAPDBResourceExistsError, match=re.escape(msg)):
        db.project_server.create(session, project_server_create)


def test_list(session: Session, project_server: ProjectServer) -> None:
    project_servers = db.project_server.list_(session)
    assert project_server.id in (_.id for _ in project_servers)


def test_get_by_id(session: Session, project_server: ProjectServer) -> None:
    project_server_get = db.project_server.get_by_id(session, project_server.id)
    assert project_server == project_server_get


def test_get_by_id_unknown_failure(session: Session) -> None:
    id_ = random.randint(100000, 200000)
    msg = f"The project server '{id_}' does not exist."
    with pytest.raises(ESAPDBResourceNotFoundError, match=re.escape(msg)):
        db.project_server.get_by_id(session, id_)


def test_delete_by_id(session: Session) -> None:
    project_server = db.project_server.create(session, fake_project_server())
    db.project_server.delete_by_id(session, project_server.id)
    msg = f"The project server '{project_server.id}' does not exist."
    with pytest.raises(ESAPDBResourceNotFoundError, match=re.escape(msg)):
        db.project_server.get_by_id(session, project_server.id)
    db.project_server.delete_by_id(session, project_server.id)
