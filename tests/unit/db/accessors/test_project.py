import random
import re

import pytest
from sqlalchemy.orm import Session

from app import db
from app.exceptions import ESAPDBResourceExistsError, ESAPDBResourceNotFoundError
from app.helpers import uid
from app.schemas import Project

from .helpers import fake_project


def test_create(session: Session) -> None:
    project_create = fake_project()
    project = db.project.create(session, project_create)
    assert project.id is not None
    for field in project_create.__fields__:
        assert getattr(project_create, field) == getattr(project, field)


def test_create_duplicate_failure(session: Session) -> None:
    project_create = fake_project()
    db.project.create(session, project_create)
    msg = f"The project '{project_create.name}' already exists."
    with pytest.raises(ESAPDBResourceExistsError, match=re.escape(msg)):
        db.project.create(session, project_create)


def test_list(session: Session, project: Project) -> None:
    projects = db.project.list_(session)
    assert project.name in (_.name for _ in projects)


def test_get_by_id(session: Session, project: Project) -> None:
    project_get = db.project.get_by_id(session, project.id)
    assert project == project_get


def test_get_by_id_unknown_failure(session: Session) -> None:
    id_ = random.randint(100000, 200000)
    msg = f"The project '{id_}' does not exist."
    with pytest.raises(ESAPDBResourceNotFoundError, match=re.escape(msg)):
        db.project.get_by_id(session, id_)


def test_get_by_name(session: Session) -> None:
    project = db.project.create(session, fake_project())
    project_get = db.project.get_by_name(session, project.name)
    assert project == project_get


def test_get_by_name_unknown_failure(session: Session) -> None:
    id_ = uid()
    msg = f"The project '{id_}' does not exist."
    with pytest.raises(ESAPDBResourceNotFoundError, match=re.escape(msg)):
        db.project.get_by_name(session, id_)


def test_delete_by_id(session: Session) -> None:
    project = db.project.create(session, fake_project())
    db.project.delete_by_id(session, project.id)
    msg = f"The project '{project.id}' does not exist."
    with pytest.raises(ESAPDBResourceNotFoundError, match=re.escape(msg)):
        db.project.get_by_id(session, project.id)
    db.project.delete_by_id(session, project.id)


def test_delete_by_name(session: Session) -> None:
    project = db.project.create(session, fake_project())
    db.project.delete_by_name(session, project.name)
    msg = f"The project '{project.name}' does not exist."
    with pytest.raises(ESAPDBResourceNotFoundError, match=re.escape(msg)):
        db.project.get_by_name(session, project.name)
    db.project.delete_by_name(session, project.name)
