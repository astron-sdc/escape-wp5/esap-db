from typing import Iterator

import pytest
from sqlalchemy.orm import Session

from app import db
from app.db.dbadmin import begin_session
from app.exceptions import ESAPDBResourceNotFoundError
from app.schemas import Dataset, Project, ProjectServer

from .helpers import fake_dataset, fake_project, fake_project_server


@pytest.fixture
def project_server(session: Session) -> Iterator[ProjectServer]:
    project_server_create = fake_project_server()
    project_server = db.project_server.create(session, project_server_create)
    yield project_server
    try:
        db.project_server.delete_by_id(session, project_server.id)
    except Exception:
        pass


@pytest.fixture
def project(session: Session) -> Iterator[Project]:
    project_create = fake_project()
    yield db.project.create(session, project_create)

    # Some errors are caught with pytest.raises and the session may be invalid.
    with begin_session() as session:
        try:
            datasets = db.dataset.list_by_project(session, project_create.name)
        except ESAPDBResourceNotFoundError:
            return
        for dataset in datasets:
            db.dataset.delete_by_name(session, dataset.name)
        db.project.delete_by_name(session, project_create.name)


@pytest.fixture
def dataset(session: Session, project: Project) -> Iterator[Dataset]:
    dataset_create = fake_dataset(project.name)
    yield db.dataset.create(session, dataset_create)

    # Some errors are caught with pytest.raises and the session may be invalid.
    with begin_session() as session:
        try:
            tables = db.table.list_by_dataset(session, dataset_create.name)
        except ESAPDBResourceNotFoundError:
            return
        for table in tables:
            db.table.delete_by_name(session, table.name)
        db.dataset.delete_by_name(session, dataset_create.name)
