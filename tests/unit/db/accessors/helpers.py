from uuid import uuid4

from app.helpers import uid
from app.schemas import DatasetCreate, ProjectCreate, ProjectServerCreate, TableCreate


def fake_project_server() -> ProjectServerCreate:
    return ProjectServerCreate(
        uri=f'postgresql://{uuid4()}:{uuid4()}@{uuid4()}.org',
        max_size=20000,
        available_size=10000,
    )


def fake_project() -> ProjectCreate:
    return ProjectCreate(
        name=uid('project'), description='mljmlkjsdf', uri='http://xyz.com'
    )


def fake_dataset(project_name: str) -> DatasetCreate:
    return DatasetCreate(
        name=f'{project_name}.{uid("dataset")}', description='mljmlkjsdf'
    )


def fake_table(dataset_name: str) -> TableCreate:
    return TableCreate(name=f'{dataset_name}.{uid("table")}', description='mljmlkjsdf')
