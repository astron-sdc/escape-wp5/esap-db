import re

import pytest
from sqlalchemy.orm import Session

from app import db
from app.exceptions import ESAPDBResourceExistsError, ESAPDBResourceNotFoundError
from app.schemas import Project

from .helpers import fake_dataset


def test_create(session: Session, project: Project) -> None:
    dataset_create = fake_dataset(project.name)
    dataset = db.dataset.create(session, dataset_create)
    assert dataset.id is not None
    assert dataset.project_id == project.id
    for field in dataset_create.__fields__:
        assert getattr(dataset_create, field) == getattr(dataset, field)


def test_create_duplicate_failure(session: Session, project: Project) -> None:
    dataset_create = fake_dataset(project.name)
    db.dataset.create(session, dataset_create)
    msg = f"The dataset '{dataset_create.name}' already exists."
    with pytest.raises(ESAPDBResourceExistsError, match=re.escape(msg)):
        db.dataset.create(session, dataset_create)


def test_list(session: Session, project: Project) -> None:
    dataset = fake_dataset(project.name)
    db.dataset.create(session, dataset)
    datasets = db.dataset.list_by_project(session, project.name)
    assert dataset.name in (_.name for _ in datasets)


def test_get_by_name(session: Session, project: Project) -> None:
    dataset = db.dataset.create(session, fake_dataset(project.name))
    dataset_get = db.dataset.get_by_name(session, dataset.name)
    assert dataset == dataset_get


def test_get_by_name_failure(session: Session, project: Project) -> None:
    msg = "The dataset name 'my_dataset' is not of the form 'project.dataset'."
    with pytest.raises(ValueError, match=re.escape(msg)):
        db.dataset.get_by_name(session, 'my_dataset')


def test_delete_by_name(session: Session, project: Project) -> None:
    dataset = db.dataset.create(session, fake_dataset(project.name))
    db.dataset.delete_by_name(session, dataset.name)
    with pytest.raises(ESAPDBResourceNotFoundError):
        assert db.dataset.get_by_name(session, dataset.name)
    db.dataset.delete_by_name(session, dataset.name)


def test_delete_by_name_failure(session: Session, project: Project) -> None:
    msg = "The dataset name 'my_dataset' is not of the form 'project.dataset'."
    with pytest.raises(ValueError, match=re.escape(msg)):
        db.dataset.delete_by_name(session, 'my_dataset')
