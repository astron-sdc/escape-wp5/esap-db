from typing import Iterator

import pytest
from sqlalchemy.orm import Session

from app.db.dbadmin import begin_session


@pytest.fixture
def session() -> Iterator[Session]:
    with begin_session() as session:
        yield session
