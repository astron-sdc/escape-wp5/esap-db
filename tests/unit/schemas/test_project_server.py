import pytest

from app.schemas import ProjectServerCreate


@pytest.mark.parametrize(
    'data',
    [
        {
            'uri': 'postgresql://user:pass@xyz.org',
            'max_size': 100,
            'available_size': 100,
        },
        {'uri': 'postgresql://user:pass@xyz.org', 'max_size': 100},
        {'uri': 'postgresql://user:pass@xyz.org'},
    ],
)
def test_project_server_success(data: dict) -> None:
    server = ProjectServerCreate(**data)
    if server.max_size is None:
        assert server.max_size is None
    else:
        assert server.available_size == server.max_size
