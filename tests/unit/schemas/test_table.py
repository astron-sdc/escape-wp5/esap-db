import re

import pytest

from app.schemas import TableCreate
from app.schemas.helpers import IDENTIFIER_MAX_LENGTH


@pytest.mark.parametrize(
    'data',
    [
        {'name': 'table_id'},
        {'name': 'dataset_id.table_id'},
        {'name': 'project_id.dataset_id.table_id'},
        {'name': '_table_id'},
        {'name': 'dataset_id._table_id'},
        {'name': 'project_id.dataset_id._table_id'},
        {'name': 'table_id$'},
        {'name': 'dataset_id.table_id$'},
        {'name': 'project_id.dataset_id.table_id$'},
    ],
)
def test_table_success(data: dict) -> None:
    TableCreate(**data)


@pytest.mark.parametrize(
    'data, msg',
    [
        ({'name': ''}, 'Valid characters are alphanumerical'),
        ({'name': 'project_id.dataset_id.'}, 'Valid characters are alphanumerical'),
        (
            {'name': (IDENTIFIER_MAX_LENGTH + 1) * 'a'},
            'The table name length is greater than',
        ),
        (
            {'name': 'project_id.dataset_id.' + (IDENTIFIER_MAX_LENGTH + 1) * 'a'},
            'The table name length is greater than',
        ),
        (
            {'name': 'xyz.project_id.dataset_id.table_id'},
            'Valid characters are alphanumerical',
        ),
        ({'name': '0table_id'}, 'Valid characters are alphanumerical'),
        (
            {'name': 'project_id.dataset_id.0table_id'},
            'Valid characters are alphanumerical',
        ),
        (
            {'name': 'project_id.dataset_id.table_id&'},
            'Valid characters are alphanumerical',
        ),
    ],
)
def test_dataset_failure(data: dict, msg: str) -> None:
    with pytest.raises(ValueError, match=re.escape(msg)):
        TableCreate(**data)
