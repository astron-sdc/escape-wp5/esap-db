import re

import pytest

from app.schemas import ProjectCreate
from app.schemas.helpers import IDENTIFIER_MAX_LENGTH


@pytest.mark.parametrize(
    'data',
    [
        {'name': 'project_id'},
        {'name': 'project_id$'},
    ],
)
def test_project_success(data: dict) -> None:
    project = ProjectCreate(**data)
    assert project.max_size > 0


@pytest.mark.parametrize(
    'data, msg',
    [
        ({'name': ''}, 'Valid characters are alphanumerical'),
        (
            {'name': (IDENTIFIER_MAX_LENGTH + 1) * 'a'},
            'The project name length is greater than',
        ),
        ({'name': 'project_id.dataset_id'}, 'Valid characters are alphanumerical'),
        ({'name': '0project_id'}, 'Valid characters are alphanumerical'),
        ({'name': 'project_id&'}, 'Valid characters are alphanumerical'),
    ],
)
def test_project_failure(data: dict, msg: str) -> None:
    with pytest.raises(ValueError, match=re.escape(msg)):
        ProjectCreate(**data)
