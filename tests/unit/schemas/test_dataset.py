import re

import pytest

from app.schemas import DatasetCreate
from app.schemas.helpers import IDENTIFIER_MAX_LENGTH


@pytest.mark.parametrize(
    'data',
    [
        {'name': 'dataset_id'},
        {'name': 'project_id.dataset_id'},
        {'name': '_dataset_id'},
        {'name': 'project_id._dataset_id'},
        {'name': 'dataset_id$'},
        {'name': 'project_id.dataset_id$'},
    ],
)
def test_dataset_success(data: dict) -> None:
    DatasetCreate(**data)


@pytest.mark.parametrize(
    'data, msg',
    [
        ({'name': ''}, 'Valid characters are alphanumerical'),
        ({'name': 'project_id.'}, 'Valid characters are alphanumerical'),
        (
            {'name': (IDENTIFIER_MAX_LENGTH + 1) * 'a'},
            'The dataset name length is greater than',
        ),
        (
            {'name': 'project_id.' + (IDENTIFIER_MAX_LENGTH + 1) * 'a'},
            'The dataset name length is greater than',
        ),
        (
            {'name': 'project_id.dataset_id.table_id'},
            'Valid characters are alphanumerical',
        ),
        ({'name': '0dataset_id'}, 'Valid characters are alphanumerical'),
        ({'name': 'project_id.0dataset_id'}, 'Valid characters are alphanumerical'),
        ({'name': 'project_id.dataset_id&'}, 'Valid characters are alphanumerical'),
    ],
)
def test_dataset_failure(data: dict, msg: str) -> None:
    with pytest.raises(ValueError, match=re.escape(msg)):
        DatasetCreate(**data)
