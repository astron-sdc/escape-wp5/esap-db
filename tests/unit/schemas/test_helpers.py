import string

from app.schemas.helpers import normalize_identifier


def test_normalize_identifier() -> None:
    normalized = normalize_identifier(string.printable).replace('_', '')
    assert (
        normalized == '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ$'
    )


def test_normalize_underscore() -> None:
    assert normalize_identifier('___') == '___'


def test_normalize_digit() -> None:
    assert normalize_identifier('012') == '_012'
