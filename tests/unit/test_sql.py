import re

import pytest

from app.exceptions import ESAPDBNotImplementedError, ESAPDBValidationError
from app.sql import SQLQuery


@pytest.mark.parametrize(
    'value, source_project, normalized',
    [
        (' ;; SELECT * FROM a.b;;;', None, 'SELECT * FROM a.b'),
        ("SELECT * FROM a WHERE x=';';", None, "SELECT * FROM a WHERE x=';'"),
        ('SELECT a FROM x.y.z', 'x', 'SELECT a FROM y.z'),
        (
            'SELECT a FROM y.z WHERE (SELECT b FROM x.y.z) = 1',
            'x',
            'SELECT a FROM y.z WHERE (SELECT b FROM y.z) = 1',
        ),
    ],
)
def test_sql_query_success(value: str, source_project: str, normalized: str) -> None:
    sql_query = SQLQuery(value)
    assert sql_query.source_project == source_project
    assert sql_query.normalized == normalized


@pytest.mark.parametrize(
    'value, msg',
    [
        (
            'SELECT * FROM a.b; DROP SCHEMA a;',
            'Multiple statements are not allowed in a query.',
        ),
        (
            'DROP TABLE x;',
            'The SQL query statement does not start with a SELECT clause.',
        ),
        (
            'SELECT * FROM w.x.y.z',
            "The table identifier is not of the form 'project.dataset.table'",
        ),
        (
            """SELECT AVG(number_of_students)
FROM classes
WHERE teacher_id IN (
    SELECT id
    FROM a.b.c.teachers
    WHERE subject = 'English' OR subject = 'History');""",
            "The table identifier is not of the form 'project.dataset.table'",
        ),
    ],
)
def test_sql_query_invalid_failure(value: str, msg: str) -> None:
    with pytest.raises(ESAPDBValidationError, match=re.escape(msg)):
        SQLQuery(value)


@pytest.mark.parametrize(
    'value, msg',
    [
        (
            'SELECT a FROM project1.a.b AS b JOIN project2.c.d AS d ON b.x=d.y',
            'It is not yet possible to combine tables from different projects: project1, project2',
        ),
    ],
)
def test_sql_query_not_implemented_failure(value: str, msg: str) -> None:
    with pytest.raises(ESAPDBNotImplementedError, match=re.escape(msg)):
        SQLQuery(value)
